Final project for the Mobile Game Development course.

Tileset by Tilation
https://tilation.itch.io/16x16-small-indoor-tileset

Characters by Aleksandr Makarov
https://iknowkingrabbit.itch.io/heroic-creature-pack

Buttons by PAM
http://pixelartmaker.com/art/3bf913ce818ddfe

Font by OmegaPC777
https://www.dafont.com/it/font-comment.php?file=pixeled
