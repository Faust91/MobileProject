using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamScreen : MonoBehaviour
{
    private Animator teamAnimator;
    private Text teamText;
    private TurnManager turnManager;

    private void Awake()
    {
        teamAnimator = GetComponent<Animator>();
        teamText = GetComponent<Text>();
        turnManager = FindObjectOfType<TurnManager>();
    }

    public void ShowTeam(TeamEnum team)
    {
        teamText.text = team.ToString();
        teamAnimator.SetTrigger("PopUp");
    }

    public void AnimationEnded()
    {
        turnManager.UnpauseAndNext();
    }

}
