using UnityEngine;
using UnityEngine.UI;

public class AlphaBlink : MonoBehaviour
{
    private enum StateEnum
    {
        zero, one, zeroone, onezero
    }

    [Header("Settings")]
    [SerializeField] private float alpha0Value = 0.0f;
    [SerializeField] private float alpha1Value = 1.0f;
    [SerializeField] private bool startFrom0 = true;

    [Header("Timers")]
    [SerializeField] private float alpha0Time = 0.25f;
    [SerializeField] private float alpha01Time = 1.0f;
    [SerializeField] private float alpha1Time = 7.5f;
    [SerializeField] private float alpha10Time = 0.75f;

    private Text text;

    private Color color0;
    private Color color1;

    private float timeCount;
    private float waitForTime;
    private StateEnum alphaState;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    private void Start()
    {
        color0 = new Color(text.color.r, text.color.g, text.color.b, alpha0Value);
        color1 = new Color(text.color.r, text.color.g, text.color.b, alpha1Value);

        if (startFrom0)
        {
            text.color = color0;
        }
        else
        {
            text.color = color1;
        }

        // initialize variables
        timeCount = 0.0f;
        waitForTime = startFrom0 ? alpha0Time : alpha1Time;
        alphaState = startFrom0 ? StateEnum.zero : StateEnum.one;
    }

    private void Update()
    {
        timeCount += Time.deltaTime;
        while (timeCount >= waitForTime)
        {
            timeCount -= waitForTime;
            switch (alphaState)
            {
                case StateEnum.zero:
                    alphaState = StateEnum.zeroone;
                    waitForTime = alpha01Time;
                    break;
                case StateEnum.zeroone:
                    alphaState = StateEnum.one;
                    waitForTime = alpha1Time;
                    text.color = color1;
                    break;
                case StateEnum.one:
                    alphaState = StateEnum.onezero;
                    waitForTime = alpha10Time;
                    break;
                case StateEnum.onezero:
                    alphaState = StateEnum.zero;
                    waitForTime = alpha0Time;
                    text.color = color0;
                    break;
            }
        }

        // handle smooth transitions
        if (alphaState == StateEnum.zeroone)
        {
            text.color = Color.Lerp(color0, color1, timeCount / waitForTime);
        }
        else if (alphaState == StateEnum.onezero)
        {
            text.color = Color.Lerp(color1, color0, timeCount / waitForTime);
        }
    }
}
