using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeMenuBackground : MonoBehaviour
{
    [SerializeField] private Toggle arena1Toggle;
    [SerializeField] private Toggle arena2Toggle;
    [SerializeField] private Toggle arena3Toggle;

    [SerializeField] private Toggle team1Toggle;
    [SerializeField] private Toggle team2Toggle;

    [SerializeField] private Sprite arena1;
    [SerializeField] private Sprite arena2;
    [SerializeField] private Sprite arena3;

    [SerializeField] private Image background;
    [SerializeField] private GameObject rulesTeam1;
    [SerializeField] private GameObject rulesTeam2;
    [SerializeField] private GameObject team1Indicator;
    [SerializeField] private GameObject team2Indicator;

    private void Awake()
    {
        background.sprite = null;
        rulesTeam1.SetActive(false);
        rulesTeam2.SetActive(false);
        team1Indicator.SetActive(false);
        team2Indicator.SetActive(false);
    }

    private void Start()
    {
        ChangeArena();
        ChangeRules();
    }

    public void ChangeRules()
    {
        if (team1Toggle.isOn)
        {
            rulesTeam1.SetActive(true);
            rulesTeam2.SetActive(false);
            team1Indicator.SetActive(true);
            team2Indicator.SetActive(false);
        }
        else if (team2Toggle.isOn)
        {
            rulesTeam1.SetActive(false);
            rulesTeam2.SetActive(true);
            team1Indicator.SetActive(false);
            team2Indicator.SetActive(true);
        }
        else
        {
            rulesTeam1.SetActive(false);
            rulesTeam2.SetActive(false);
            team1Indicator.SetActive(false);
            team2Indicator.SetActive(false);
        }
    }

    public void ChangeArena()
    {
        if (arena1Toggle.isOn)
        {
            background.sprite = arena1;
        }
        else if (arena2Toggle.isOn)
        {
            background.sprite = arena2;
        }
        else if (arena3Toggle.isOn)
        {
            background.sprite = arena3;
        }
        else
        {
            background.sprite = null;
        }
    }
}
