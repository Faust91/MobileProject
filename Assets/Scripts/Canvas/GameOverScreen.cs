using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour
{
    [SerializeField] private Text gameOverText;

    private void Start()
    {
        Color newColor = new Color(gameOverText.color.r, gameOverText.color.g, gameOverText.color.b, 0.0f);
        gameOverText.color = newColor;
    }

    public void ShowGameOverScreen(TeamEnum team)
    {
        gameOverText.text = team.ToString() + " wins!";
        Color newColor = new Color(gameOverText.color.r, gameOverText.color.g, gameOverText.color.b, 1.0f);
        gameOverText.color = newColor;
    }

    public void HideGameOverScreen()
    {
        Color newColor = new Color(gameOverText.color.r, gameOverText.color.g, gameOverText.color.b, 0.0f);
        gameOverText.color = newColor;
    }
}
