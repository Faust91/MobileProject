using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeactivatableButton : MonoBehaviour
{
    public Button GetButton()
    {
        return this.gameObject.GetComponent<Button>();
    }
}
