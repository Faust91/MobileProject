using UnityEngine;
using UnityEngine.UI;

public abstract class AbstractMessage : MonoBehaviour
{
    public Text GetText()
    {
        return this.gameObject.GetComponent<Text>();
    }
}
