using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum CharacterTypeEnum
{
    DEFAULT, KING
}

public enum UnitTypeEnum
{
    Commander, Knight, Archer, Infantryman, Pikeman
}

[CreateAssetMenu(fileName = "CharacterStats", menuName = "Character")]

public class CharacterStats : ScriptableObject
{
    [Header("Info")]
    public CharacterTypeEnum CharacterType;
    public UnitTypeEnum UnitType;

    [Header("Stats")]
    public float Health;
    public float Attack;
    public int Movement;
    public int WeaponRange;

    [Header("Bonus")]
    public float BonusCommander = 1.0f;
    public float BonusKnight = 1.0f;
    public float BonusArcher = 1.0f;
    public float BonusInfantryman = 1.0f;
    public float BonusPikeman = 1.0f;

    public static void CopyStats(CharacterStats source, CharacterStats target)
    {
        target.CharacterType = source.CharacterType;
        target.UnitType = source.UnitType;

        target.Health = source.Health;
        target.Attack = source.Attack;
        target.Movement = source.Movement;
        target.WeaponRange = source.WeaponRange;

        target.BonusCommander = source.BonusCommander;
        target.BonusKnight = source.BonusKnight;
        target.BonusArcher = source.BonusArcher;
        target.BonusInfantryman = source.BonusInfantryman;
        target.BonusPikeman = source.BonusPikeman;
    }

    public static float GetDamageAmount(CharacterStats attacker, CharacterStats target)
    {
        float amount = attacker.Attack;

        switch (target.UnitType)
        {
            case UnitTypeEnum.Commander:
                amount *= attacker.BonusCommander;
                break;
            case UnitTypeEnum.Knight:
                amount *= attacker.BonusKnight;
                break;
            case UnitTypeEnum.Archer:
                amount *= attacker.BonusArcher;
                break;
            case UnitTypeEnum.Infantryman:
                amount *= attacker.BonusInfantryman;
                break;
            case UnitTypeEnum.Pikeman:
                amount *= attacker.BonusPikeman;
                break;
            default:
                break;
        }

        return amount;
    }
}
