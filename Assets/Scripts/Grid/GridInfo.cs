using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridInfo : MonoBehaviour
{
    public int HalfWidth = 10;
    public int HalfHeight = 5;
}
