using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : IEquatable<Cell>
{
    public enum CellType
    {
        NONE, FLOOR, ENEMY, ALLY, PLAYER
    }

    public Vector3Int Position;
    public CellType Type;
    // excluded from equals
    public int Distance;

    public Cell() { }

    public Cell(Vector3Int position, CellType type, int distance)
    {
        this.Position = position;
        this.Type = type;
        this.Distance = distance;
    }

    public override bool Equals(object obj)
    {
        return Equals(obj as Cell);

    }

    public bool Equals(Cell other)
    {
        return (this.Position.Equals(other.Position) && this.Type == other.Type);
    }

    public override int GetHashCode()
    {
        return Position.GetHashCode() ^ Type.GetHashCode();
    }

    public bool IsAdjacent(Cell other)
    {
        return ((Position.x == other.Position.x && ((Position.y == other.Position.y + 1) || (Position.y == other.Position.y - 1)))
            || (Position.y == other.Position.y && ((Position.x == other.Position.x + 1) || (Position.x == other.Position.x - 1))));
    }
}
