using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GridController : MonoBehaviour
{
    [Header("Tilemaps")]
    [SerializeField] private Tilemap walkableTilemap;
    [SerializeField] private Tilemap notWalkableTilemap;

    private TeamsManager teamManager;

    private void Awake()
    {
        teamManager = FindObjectOfType<TeamsManager>();
    }

    public List<Cell> GetReachableCells(Vector3 position, Character player, bool move, bool attack)
    {
        Vector3Int currentCell = GetCellPosition(position, walkableTilemap);
        return GetInteractablePositions(currentCell, player, move, attack);
    }

    private List<Cell> GetInteractablePositions(Vector3Int currentCell, Character player, bool move, bool attack)
    {
        int maxWalkDistance = move ? player.GetStats().Movement : 0;
        int maxAttackDistance = attack ? player.GetStats().WeaponRange : 0;
        int maxDistance = Mathf.Max(maxWalkDistance, maxAttackDistance);

        List<Cell> retList = new List<Cell>();
        retList.Add(new Cell(currentCell, Cell.CellType.PLAYER, 0));
        List<Vector3Int> cellsToCheck = new List<Vector3Int>();
        cellsToCheck.Add(currentCell);
        for (int distance = 1; distance <= maxDistance; distance++)
        {
            List<Cell> newNeighborsPositions = new List<Cell>();
            foreach (Vector3Int cell in cellsToCheck)
            {
                bool floorMask = distance <= maxWalkDistance;
                bool enemiesMask = distance <= maxAttackDistance;
                List<Cell> currentNeighborsPositions = GetInteractableNeighborsPositions(cell, player, distance, floorMask, enemiesMask);
                List<Cell> currentEmptyPositions = GetInteractableNeighborsPositions(cell, player, distance, true, false);
                currentNeighborsPositions.RemoveAll(x => (retList.Contains(x)));
                newNeighborsPositions.AddRange(currentEmptyPositions);
                retList.AddRange(currentNeighborsPositions);
            }
            cellsToCheck.Clear();
            foreach (Cell floorCell in newNeighborsPositions.FindAll(x => x.Type == Cell.CellType.FLOOR))
            {
                cellsToCheck.Add(floorCell.Position);
            }
        }
        return retList;
    }

    private List<Cell> GetInteractableNeighborsPositions(Vector3Int currentCell, Character player, int distance, bool floorMask, bool enemiesMask)
    {
        List<Cell> interactablePositions = GetCharactersCellsPositions(teamManager.FindLivingCharacters(), walkableTilemap, player, distance);

        List<Vector3Int> walkables = GetNeighborsPositions(currentCell, walkableTilemap);
        List<Vector3Int> notWalkables = GetNeighborsPositions(currentCell, notWalkableTilemap);

        walkables.RemoveAll(x => notWalkables.Contains(x));

        List<Cell> cells = new List<Cell>();
        foreach (Vector3Int walkablePos in walkables)
        {
            Cell interactableCell = interactablePositions.Find(x => x.Position.Equals(walkablePos));
            if (floorMask)
            {
                if (interactableCell == null)
                {
                    cells.Add(new Cell(walkablePos, Cell.CellType.FLOOR, distance));
                }
            }
            if (enemiesMask)
            {
                if (interactableCell != null)
                {
                    cells.Add(interactableCell);
                }
            }
        }

        return cells;
    }

    private List<Vector3Int> GetNeighborsPositions(Vector3Int currentCell, Tilemap tilemap)
    {
        Vector3Int upCell = new Vector3Int(currentCell.x, currentCell.y + 1, currentCell.z);
        Vector3Int downCell = new Vector3Int(currentCell.x, currentCell.y - 1, currentCell.z);
        Vector3Int rightCell = new Vector3Int(currentCell.x + 1, currentCell.y, currentCell.z);
        Vector3Int leftCell = new Vector3Int(currentCell.x - 1, currentCell.y, currentCell.z);

        List<Vector3Int> neighborsPos = new List<Vector3Int>();

        if (tilemap.GetTile(upCell))
        {
            neighborsPos.Add(upCell);
        }
        if (tilemap.GetTile(downCell))
        {
            neighborsPos.Add(downCell);
        }
        if (tilemap.GetTile(rightCell))
        {
            neighborsPos.Add(rightCell);
        }
        if (tilemap.GetTile(leftCell))
        {
            neighborsPos.Add(leftCell);
        }

        return neighborsPos;
    }

    private List<Cell> GetCharactersCellsPositions(List<Character> characters, Tilemap tilemap, Character player, int distance)
    {
        List<Cell> retList = new List<Cell>();
        if (characters != null)
        {
            foreach (Character character in characters)
            {
                retList.Add(new Cell(GetCellPosition(character.gameObject.transform.position, tilemap), character.SameTeam(player) ? (character.Equals(player) ? Cell.CellType.PLAYER : Cell.CellType.ALLY) : Cell.CellType.ENEMY, distance));
            }
        }
        return retList;
    }

    private Vector3Int GetCellPosition(Vector3 worldPosition, Tilemap tilemap)
    {
        return tilemap.WorldToCell(worldPosition);
    }

    public Vector3Int GetCellPosition(Vector3 worldPosition)
    {
        return GetCellPosition(worldPosition, walkableTilemap);
    }

    public Vector3 GetWorldPosition(Vector3Int cellPosition)
    {
        return GetWorldPosition(cellPosition, walkableTilemap);
    }

    private Vector3 GetWorldPosition(Vector3Int cellPosition, Tilemap tilemap)
    {
        return tilemap.CellToWorld(cellPosition);
    }

}
