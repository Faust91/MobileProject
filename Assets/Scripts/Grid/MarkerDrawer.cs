using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MarkerDrawer : MonoBehaviour
{
    [Header("Tilemaps")]
    [SerializeField] private Tilemap markersTilemap;

    [Header("Markers")]
    [SerializeField] private TileBase greenMarker;
    [SerializeField] private TileBase redMarker;
    [SerializeField] private TileBase whiteMarker;

    private TeamsManager teamsManager;
    private GridController gridController;

    private Coroutine drawUsableCharactersC;
    private Coroutine drawInteractableCellsC;

    private void Awake()
    {
        teamsManager = FindObjectOfType<TeamsManager>();
        gridController = GetComponent<GridController>();
    }

    public void MarkUsableCharacters(TeamEnum team, bool cleanBefore)
    {
        if (drawUsableCharactersC != null)
        {
            StopCoroutine(drawUsableCharactersC);
        }
        drawUsableCharactersC = StartCoroutine(DrawUsableCharactersCoroutine(team, cleanBefore));
    }

    private IEnumerator DrawUsableCharactersCoroutine(TeamEnum team, bool cleanBefore)
    {
        if (cleanBefore)
        {
            ClearMarkers();
        }
        yield return null;
        DrawUsableCharacters(team);
    }

    public void DrawUsableCharacters(TeamEnum team)
    {
        foreach (Character character in teamsManager.FindLivingCharacters().FindAll(x => x.Team == team))
        {
            if (character.Team == team && character.CanMoveOrAttack())
            {
                Mark(gridController.GetCellPosition(character.gameObject.transform.position), greenMarker);
            }
        }
    }

    public void MarkInteractableCells(List<Cell> cells, bool cleanBefore)
    {
        if (drawInteractableCellsC != null)
        {
            StopCoroutine(drawInteractableCellsC);
        }
        drawInteractableCellsC = StartCoroutine(DrawInteractableCellsCoroutine(cells, cleanBefore));
    }

    private IEnumerator DrawInteractableCellsCoroutine(List<Cell> cells, bool cleanBefore)
    {
        if (cleanBefore)
        {
            ClearMarkers();
        }
        yield return null;
        DrawInteractableCells(cells);
    }

    public void ClearMarkers()
    {
        markersTilemap.ClearAllTiles();
    }

    private void DrawInteractableCells(List<Cell> cells)
    {
        foreach (Cell cell in cells)
        {
            if (cell.Type == Cell.CellType.FLOOR)
            {
                Mark(cell.Position, whiteMarker);
            }
            else if (cell.Type == Cell.CellType.ENEMY)
            {
                Mark(cell.Position, redMarker);
            }
        }
    }

    private void Mark(List<Vector3Int> positions, TileBase marker)
    {
        foreach (Vector3Int pos in positions)
        {
            Mark(pos, marker);
        }
    }

    private void Mark(Vector3Int position, TileBase marker)
    {
        markersTilemap.SetTile(position, marker);
    }
}
