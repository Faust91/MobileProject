using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputClick : MonoBehaviour
{
    public delegate void TileClick(Vector3Int tilePosition);
    public event TileClick OnTileClick;
    private GridController grid;

    private void Awake()
    {
        grid = GetComponent<GridController>();
    }

    public void ClearOnTileClick()
    {
        if (OnTileClick != null && OnTileClick.GetInvocationList() != null)
        {
            foreach (Delegate d in OnTileClick.GetInvocationList())
            {
                OnTileClick -= (TileClick)d;
            }
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3Int pos = GetClickedCelPosition();
            OnTileClick?.Invoke(pos);
        }
    }

    public Vector3Int GetClickedCelPosition()
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 noZ = new Vector3(pos.x, pos.y);
        return grid.GetCellPosition(noZ);
    }
}
