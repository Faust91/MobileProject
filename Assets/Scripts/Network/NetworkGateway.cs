using Assets.Scripts.Network.NetworkModel;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum HTTPMethods
{
    GET,
    POST,
    PUT,
    DELETE
}

public struct JsonDateTime
{
    public long value;
    public static implicit operator DateTime(JsonDateTime jdt)
    {
        Debug.Log("Converted to time");
        return DateTime.FromFileTimeUtc(jdt.value);
    }
    public static implicit operator JsonDateTime(DateTime dt)
    {
        Debug.Log("Converted to JDT");
        JsonDateTime jdt = new JsonDateTime();
        jdt.value = dt.ToFileTimeUtc();
        return jdt;
    }
}

public static class NetworkGateway
{

    private static string baseurl = null;

    private static string BearerToken;

    #region Config

    public static void SetServer(String serverUrl)
    {
        baseurl = serverUrl;
    }

    public static void ForceToken(string token)
    {
        BearerToken = token;
    }

    #endregion

    #region Player
    public static IEnumerator GetPlayers(Action<ResponseContainer<List<Player>>> callbackPlayers)
    {
        yield return Request(HTTPMethods.GET, baseurl + "player", json =>
        {
            callbackPlayers?.Invoke(JsonUtility.FromJson<ResponseContainer<List<Player>>>(json));
        });
    }

    public static IEnumerator Login(AuthenticateModel LogInForm, Action<ResponseContainer<AuthenticatedWithToken>> callbackLogIn)
    {
        yield return Request(HTTPMethods.POST, baseurl + "player" + "/login/", json =>
        {
            ResponseContainer<AuthenticatedWithToken> response = JsonUtility.FromJson<ResponseContainer<AuthenticatedWithToken>>(json);
            if (response.Response != null)
                BearerToken = response.Response.Token;
            callbackLogIn?.Invoke(response);
        }, JsonUtility.ToJson(LogInForm));
    }

    public static IEnumerator SignIn(AuthenticateModel RegistrationForm, Action<ResponseContainer<Player>> callbackLogIn)
    {
        yield return Request(HTTPMethods.POST, baseurl + "player" + "/signin/", json =>
        {
            callbackLogIn?.Invoke(JsonUtility.FromJson<ResponseContainer<Player>>(json));
        }, JsonUtility.ToJson(RegistrationForm));
    }
    #endregion

    #region Match

    public static IEnumerator MyMatches(Action<ResponseContainer<List<MatchResume>>> callbackMyMatches)
    {
        yield return Request(HTTPMethods.GET, baseurl + "match/mymatches", json =>
        {
            callbackMyMatches?.Invoke(JsonUtility.FromJson<ResponseContainer<List<MatchResume>>>(json));
        });
    }

    public static IEnumerator History(Action<ResponseContainer<List<MatchResume>>> callbackMyMatches)
    {
        yield return Request(HTTPMethods.GET, baseurl + "match/history", json =>
        {
            callbackMyMatches?.Invoke(JsonUtility.FromJson<ResponseContainer<List<MatchResume>>>(json));
        });
    }

    public static IEnumerator AvailableMatch(Action<ResponseContainer<List<MatchResume>>> callbackAvailableMatches)
    {
        yield return Request(HTTPMethods.GET, baseurl + "match/availablematches", json =>
        {
            callbackAvailableMatches?.Invoke(JsonUtility.FromJson<ResponseContainer<List<MatchResume>>>(json));
        });
    }

    public static IEnumerator JoinMatch(string idMatch, Action<ResponseContainer<MatchResume>> callbackJoinMatch)
    {
        yield return Request(HTTPMethods.POST, baseurl + "match/joinMatch/" + idMatch, json =>
        {
            callbackJoinMatch?.Invoke(JsonUtility.FromJson<ResponseContainer<MatchResume>>(json));
        });
    }

    public static IEnumerator CreateMatch(MatchResume match, Action<ResponseContainer<MatchResume>> callbackAvailableMatches)
    {
        yield return Request(HTTPMethods.POST, baseurl + "match/createMatch", json =>
        {
            callbackAvailableMatches?.Invoke(JsonUtility.FromJson<ResponseContainer<MatchResume>>(json));
        }, JsonUtility.ToJson(match));
    }

    public static IEnumerator GetMatch(string matchID, Action<ResponseContainer<Match>> callbackAvailableMatches)
    {
        yield return Request(HTTPMethods.GET, baseurl + "match/GetMatch/" + matchID, json =>
        {
            callbackAvailableMatches?.Invoke(JsonUtility.FromJson<ResponseContainer<Match>>(json));
        });
    }

    public static IEnumerator DeleteMatch(string matchID, Action<ResponseContainer<Match>> callbackDeleteMatch)
    {
        yield return Request(HTTPMethods.DELETE, baseurl + "match/DeleteMatch/" + matchID, json =>
        {
            callbackDeleteMatch?.Invoke(JsonUtility.FromJson<ResponseContainer<Match>>(json));
        });
    }

    public static IEnumerator History(string matchID, Action<ResponseContainer<List<MatchResume>>> callbackHistory)
    {
        yield return Request(HTTPMethods.GET, baseurl + "match/History/" + matchID, json =>
        {
            callbackHistory?.Invoke(JsonUtility.FromJson<ResponseContainer<List<MatchResume>>>(json));
        });
    }

    #endregion

    #region InGame

    public static IEnumerator SendTurn(Turn turn, Action<ResponseContainer<TurnResume>> callbackSendTurn)
    {

        yield return Request(HTTPMethods.PUT, baseurl + "inGame/SendTurn", json =>
        {
            callbackSendTurn?.Invoke(JsonUtility.FromJson<ResponseContainer<TurnResume>>(json));
        }, JsonUtility.ToJson(turn));
    }

    public static IEnumerator InGameGetUpdates(string matchId, int turnNumber, Action<ResponseContainer<List<Turn>>> callbackSendTurn)
    {
        yield return Request(HTTPMethods.GET, baseurl + "inGame/GetUpdates/" + matchId + "/" + turnNumber, json =>
        {
            callbackSendTurn?.Invoke(JsonUtility.FromJson<ResponseContainer<List<Turn>>>(json));
        });
    }

    public static IEnumerator InGameGetUpdates(string matchId, Action<ResponseContainer<List<Turn>>> callbackSendTurn)
    {
        yield return Request(HTTPMethods.GET, baseurl + "inGame/GetUpdates/" + matchId, json =>
        {
            callbackSendTurn?.Invoke(JsonUtility.FromJson<ResponseContainer<List<Turn>>>(json));
        });
    }

    #endregion

    #region Unit

    public static IEnumerator GetUnits(Action<ResponseContainer<List<Unit>>> callbackUnits)
    {
        yield return Request(HTTPMethods.GET, baseurl + "unit/", json =>
        {
            callbackUnits?.Invoke(JsonUtility.FromJson<ResponseContainer<List<Unit>>>(json));
        });
    }

    #endregion


    private static IEnumerator Request(HTTPMethods method, string uri, Action<string> callbackJson, string json = null)
    {
        using (UnityWebRequest webRequest = GetUnityWebRequest(method, uri))
        {
            if (json != null)
            {
                byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);
                webRequest.uploadHandler = new UploadHandlerRaw(jsonToSend);
                webRequest.downloadHandler = new DownloadHandlerBuffer();
                webRequest.SetRequestHeader("Content-Type", "application/json");
            }
            if (BearerToken != null)
                webRequest.SetRequestHeader("Authorization", "Bearer " + BearerToken);
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();


            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    callbackJson?.Invoke(webRequest?.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    callbackJson?.Invoke(webRequest?.downloadHandler?.text);
                    break;
                case UnityWebRequest.Result.Success:
                    callbackJson?.Invoke(webRequest?.downloadHandler?.text);
                    break;
            }
        }

    }


    private static UnityWebRequest GetUnityWebRequest(HTTPMethods method, string uri)
    {
        switch (method)
        {
            case HTTPMethods.GET:
                return UnityWebRequest.Get(uri);
            case HTTPMethods.POST:
                return UnityWebRequest.Post(uri, "POST");
            case HTTPMethods.PUT:
                return UnityWebRequest.Put(uri, "PUT");
            case HTTPMethods.DELETE:
                return UnityWebRequest.Delete(uri);
        }

        return null;
    }
}
