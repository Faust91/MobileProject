using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchPoller : MonoBehaviour
{
    private MatchClient matchClient;

    private Coroutine myMatchesCoroutineHandle;
    private Coroutine myMatchesHistoryCoroutineHandle;
    private Coroutine othersMatchesCoroutineHandle;

    private const int SECONDS_TO_WAIT_FOR = 5;

    private void Awake()
    {
        matchClient = GetComponent<MatchClient>();
    }

    public void StartPollingMyMatches()
    {
        StopPollingMyMatches();
        myMatchesCoroutineHandle = StartCoroutine(MyMatchesCoroutine());
    }

    private IEnumerator MyMatchesCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(SECONDS_TO_WAIT_FOR);
            matchClient.ShowMyMatches();
        }
    }

    public void StopPollingMyMatches()
    {
        if (myMatchesCoroutineHandle != null)
        {
            StopCoroutine(myMatchesCoroutineHandle);
        }
    }

    public void StartPollingMyMatchesHistory()
    {
        StopPollingMyMatchesHistory();
        myMatchesHistoryCoroutineHandle = StartCoroutine(MyMatchesHistoryCoroutine());
    }

    private IEnumerator MyMatchesHistoryCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(SECONDS_TO_WAIT_FOR);
            matchClient.ShowHistory();
        }
    }

    public void StopPollingMyMatchesHistory()
    {
        if (myMatchesHistoryCoroutineHandle != null)
        {
            StopCoroutine(myMatchesHistoryCoroutineHandle);
        }
    }

    public void StartPollingOthersMatches()
    {
        StopPollingOthersMatches();
        othersMatchesCoroutineHandle = StartCoroutine(OthersMatchesCoroutine());
    }

    private IEnumerator OthersMatchesCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(SECONDS_TO_WAIT_FOR);
            matchClient.ShowAvailableMatches();
        }
    }

    public void StopPollingOthersMatches()
    {
        if (othersMatchesCoroutineHandle != null)
        {
            StopCoroutine(othersMatchesCoroutineHandle);
        }
    }

    private void StopEveryPolling()
    {
        StopPollingMyMatches();
        StopPollingOthersMatches();
        StopPollingMyMatchesHistory();
    }

    private void OnDestroy()
    {
        StopEveryPolling();
    }
}
