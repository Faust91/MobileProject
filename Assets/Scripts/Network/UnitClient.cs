using Assets.Scripts.Network.NetworkModel;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitClient : MonoBehaviour
{
    private Action<bool> responseCallback = null;

    public void GetUnits()
    {
        GetUnits(null);
    }

    public void GetUnits(Action<bool> responseCallback)
    {
        this.responseCallback = responseCallback;

        CanvasItems.Instance.SetInfoText("Loading data from server...");
        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetButtonsEnabled(false);

        StartCoroutine(NetworkGateway.GetUnits(getUnitsCallback));
    }

    private void getUnitsCallback(ResponseContainer<List<Unit>> response)
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(true);

        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            responseCallback?.Invoke(false);
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            responseCallback?.Invoke(false);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            responseCallback?.Invoke(false);
            return;
        }

        foreach (Unit unit in response.Response)
        {
            List<CharacterStats> characters = MatchContainer.Instance.characterStats.FindAll(x => x.UnitType.ToString().Equals(unit.Name));
            if (characters == null || characters.Count == 0)
            {
                Debug.LogError("Unit not found!");
                // errore non bloccante per il momento
            }
            else
            {
                foreach (CharacterStats character in characters)
                {
                    //character.Health = unit.Health;
                    character.Health = unit.MaxLife;
                    character.Attack = unit.Attack;
                    character.Movement = unit.Movement;
                    //character.WeaponRange = unit.WeaponRange;
                    //character.BonusCommander = unit.BonusCommander;
                    character.BonusKnight = unit.BonusKnight;
                    character.BonusArcher = unit.BonusArcher;
                    character.BonusInfantryman = unit.BonusInfantryman;
                    character.BonusPikeman = unit.BonusPikeman;
                }
            }
        }
        responseCallback?.Invoke(true);
    }
}
