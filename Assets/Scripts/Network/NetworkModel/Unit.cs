﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
namespace Assets.Scripts.Network.NetworkModel
{
    [Serializable]
    public class Unit
    {
        public string Name;

        public int Attack;

        public int Defense;

        public int Movement;

        public float MaxLife;

        public float BonusKnight;
        public float BonusArcher;
        public float BonusInfantryman;
        public float BonusPikeman;
    }

}