﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
namespace Assets.Scripts.Network.NetworkModel
{
    [Serializable]
    public class Turn
    {
        public string ownerId;

        public string matchId;
        public int counterKey;
        public string Begin;
        public string End;

        public List<Move> Moves;

        private DateTime beginDateTime;
        private DateTime endDateTime;

        public DateTime GetBegin()
        {
            if (beginDateTime == null || beginDateTime.Equals(DateTime.MinValue))
                beginDateTime = DateTime.Parse(Begin);//.ToUniversalTime();
            return beginDateTime;
        }

        public DateTime GetEnd()
        {
            if (endDateTime == null || endDateTime.Equals(DateTime.MinValue))
                endDateTime = DateTime.Parse(End);//.ToUniversalTime();
            return endDateTime;
        }

        public void SetBegin(DateTime Begin)
        {
            this.Begin = Begin/*.ToUniversalTime()*/.ToString();
        }

        public void SetEnd(DateTime End)
        {
            this.End = End/*.ToUniversalTime()*/.ToString();
        }

        public Turn()
        {

        }

        public Turn(Player Owner, Match Match)
        {
            ownerId = Owner.Nickname;
            matchId = Match.Id;
            Moves = new List<Move>();
        }

        public Turn(string ownerId, Match Match, int counterKey)
        {
            this.ownerId = ownerId;
            matchId = Match.Id;
            this.counterKey = counterKey;
            Moves = new List<Move>();
        }

        public Turn(string ownerId, MatchResume matchResume, int counterKey)
        {
            this.ownerId = ownerId;
            matchId = matchResume.Id;
            this.counterKey = counterKey;
            Moves = new List<Move>();
        }
    }
}


