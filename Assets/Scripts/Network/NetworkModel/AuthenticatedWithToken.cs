﻿using System;

namespace Assets.Scripts.Network.NetworkModel
{
    [Serializable]
    public class AuthenticatedWithToken
    {
        public string Nickname;
        public string Token;
    }
}
