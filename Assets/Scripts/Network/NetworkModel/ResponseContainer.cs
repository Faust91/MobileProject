﻿using System;
namespace Assets.Scripts.Network.NetworkModel
{


    [Serializable]
    public class ResponseContainer<T>
    {
        public T Response;
        public string Error;

        public ResponseContainer(T Response)
        {
            this.Response = Response;
        }

        public ResponseContainer()
        {

        }
    }
}
