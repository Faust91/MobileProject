﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Network.NetworkModel
{
    [Serializable]
    public class TurnResume
    {
        public Turn turn;
        public bool IsLastTurn;
    }
}
