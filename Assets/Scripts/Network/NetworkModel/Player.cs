﻿using System;
namespace Assets.Scripts.Network.NetworkModel
{
    [Serializable]
    public class Player
    {
        public string Nickname;

        public Player()
        {

        }

        public Player(string Nickname)
        {
            this.Nickname = Nickname;
        }
    }
}
