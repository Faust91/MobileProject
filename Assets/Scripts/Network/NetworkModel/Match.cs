﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace Assets.Scripts.Network.NetworkModel
{
    [Serializable]
    public enum Army
    {
        Humans,
        Monsters
    }
    [Serializable]
    public class Match
    {
        public string Id;

        public DateTime CreatedOn;

        public int MaxTimePlayers;


        public Player PlayerHost;
        public Army PlayerHostArmy;
        public string MatchName;
        public bool Ended;
        public Player PlayerGuest;
        public Army PlayerGuestArmy;

        public Map Map;

        public List<Turn> Turns;
        public Player Winner;

        public Match()
        {

        }

        public Match(Player PlayerHost, Map Map, int MaxTimePlayers, Army PlayerHostArmy, string MatchName)
        {
            Id = default(Guid).ToString();
            Turns = new List<Turn>();
            this.MatchName = MatchName;
            this.PlayerHost = PlayerHost;
            this.Map = Map;
            this.MaxTimePlayers = MaxTimePlayers;
            this.PlayerHostArmy = PlayerHostArmy;
            if (PlayerHostArmy == Army.Humans)
                PlayerGuestArmy = Army.Monsters;
            else
                PlayerGuestArmy = Army.Humans;
        }
    }
}