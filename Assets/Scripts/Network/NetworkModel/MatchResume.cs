﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Network.NetworkModel
{
    [Serializable]
    public class MatchResume
    {
        public string Id;
        public DateTime CreatedOn;
        public int MaxTimePlayers;
        public bool Ended;
        public string MatchName;
        public string PlayerHost;
        public string PlayerGuest;
        public Army PlayerHostArmy;
        public Army PlayerGuestArmy;
        public string MapName;
        public string Winner;

    }
}
