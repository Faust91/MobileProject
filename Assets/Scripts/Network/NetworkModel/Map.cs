﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Assets.Scripts.Network.NetworkModel
{
    [Serializable]
    public class Map
    {
        public string Name;
        public int Height;
        public int Width;

        public Map()
        {

        }

        public Map(string Name, int Height, int Width)
        {
            this.Name = Name;
            this.Height = Height;
            this.Width = Width;
        }
    }
}
