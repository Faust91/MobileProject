﻿using System;

namespace Assets.Scripts.Network.NetworkModel
{
    [Serializable]
    public class AuthenticateModel
    {
        public string Username;
        public string Password;

        public AuthenticateModel(string Username, string Password)
        {
            this.Username = Username;
            this.Password = Password;
        }
    }
}
