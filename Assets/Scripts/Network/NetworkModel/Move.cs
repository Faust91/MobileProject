﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Assets.Scripts.Network.NetworkModel
{
    [Serializable]
    public enum Action
    {
        Spawn,
        Attack,
        Moving
    }

    [Serializable]
    public class Move
    {
        public string PlayerId;
        public Action Action;
        public string UnitName;
        public int DepartureX;
        public int DepartureY;
        public int ArrivalX;
        public int ArrivalY;

        public int AttackX;
        public int AttackY;

        public Move()
        {

        }

        public Move(Turn OwnedBy, Action Action, Unit Unit, int DepartureX, int DepartureY, int ArrivalX, int ArrivalY)
        {

            PlayerId = OwnedBy.ownerId;
            this.Action = Action;
            this.DepartureX = DepartureX;
            this.DepartureY = DepartureY;
            this.ArrivalX = ArrivalX;
            this.ArrivalY = ArrivalY;
            UnitName = Unit.Name;
            AttackX = -1;
            AttackY = -1;
        }

        public Move(Turn OwnedBy, Action Action, Unit Unit, int DepartureX, int DepartureY, int ArrivalX, int ArrivalY, int AttackX, int AttackY)
        {
            PlayerId = OwnedBy.ownerId;
            this.Action = Action;
            UnitName = Unit.Name;
            this.DepartureX = DepartureX;
            this.DepartureY = DepartureY;
            this.ArrivalX = ArrivalX;
            this.ArrivalY = ArrivalY;
            this.AttackX = AttackX;
            this.AttackY = AttackY;
        }
    }
}
