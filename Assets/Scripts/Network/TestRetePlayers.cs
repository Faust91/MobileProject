using Assets.Scripts.Network.NetworkModel;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRetePlayers : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        List<Player> players;
        StartCoroutine(NetworkGateway.GetPlayers(returnValue => { players = returnValue.Response; Debug.Log(players[0].Nickname); }));
        StartCoroutine(NetworkGateway.Login(new AuthenticateModel("pippo", "Sowlo"), returnValue => 
        { if (returnValue.Response != null) Debug.Log(returnValue.Response.Token); 
            else Debug.Log(returnValue.Error); 
            testNetworkToken(); }));

        /*
        Match match = new Match(new Player("Pippo"), new Map("Arena1",100,100), 600, Army.Humans, "Israele non � uno stato legittimo");
        StartCoroutine(NetworkGateway.CreateMatch(match, returnValue => { Debug.Log(returnValue.Response.Id); }));
        */
        /*
        StartCoroutine(NetworkGateway.AvailableMatch(returnValue => { Debug.Log(returnValue.Response[0].Id); }));

        StartCoroutine(NetworkGateway.JoinMatch("448967ed-fe5e-438a-97fd-0a7d2f30197c", "dio", returnValue => { Debug.Log(returnValue.Response.Id); }));
    */
    }

    // Update is called once per frame
    void Update()
    {

    }

    void testNetworkToken()
    {
        StartCoroutine(NetworkGateway.GetUnits(returnValue => { 
            Debug.Log(returnValue.Response[0].Name); }));
    }
}
