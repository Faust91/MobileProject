using Assets.Scripts.Network.NetworkModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchClient : MonoBehaviour
{
    private MatchCreatorHelper matchCreatorHelper;
    [SerializeField] private string mainMenuSceneName = "MainMenuScene";
    private RoomList roomList;
    [SerializeField] private string world1Name = "World1Scene";
    [SerializeField] private string world2Name = "World2Scene";
    [SerializeField] private string world3Name = "World3Scene";

    private void Awake()
    {
        matchCreatorHelper = GetComponent<MatchCreatorHelper>();
        roomList = FindObjectOfType<RoomList>();
    }

    public void DeleteMatch(string matchId, Button enterButton, Button deleteButton)
    {
        if (enterButton != null)
        {
            CanvasItems.Instance.AddButton(enterButton);
        }
        if (deleteButton != null)
        {
            CanvasItems.Instance.AddButton(deleteButton);
        }
        DeleteMatch(matchId);
    }

    public void DeleteMatch(string matchId)
    {
        CanvasItems.Instance.SetInfoText("Deleting match...");
        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetButtonsEnabled(false);
        StartCoroutine(NetworkGateway.DeleteMatch(matchId, deleteMatchCallback));
    }

    private void deleteMatchCallback(ResponseContainer<Match> response)
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(true);
        if (response == null)
        {
            //CanvasItems.Instance.SetErrorText("Unknown error");
            ShowMyMatches();
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            return;
        }
        if (response.Response.Id == null || response.Response.Id.Length == 0)//Non pu� mai succedere
        {
            CanvasItems.Instance.SetErrorText("Unable to join the requested match");
            return;
        }
        ShowMyMatches();
    }

    public void ShowHistory()
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetButtonsEnabled(false);
        StartCoroutine(NetworkGateway.History(myMatchesCallback));
    }

    public void ShowMyMatches()
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetButtonsEnabled(false);
        StartCoroutine(NetworkGateway.MyMatches(myMatchesCallback));
    }

    private void myMatchesCallback(ResponseContainer<List<MatchResume>> response)
    {
        CanvasItems.Instance.SetButtonsEnabled(true);
        roomList.ClearList();
        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            return;
        }
        if (response.Response == null)
        {
            return;
        }
        if (response.Response == null || response.Response.Count == 0)
        {
            return;
        }

        if (roomList != null)
        {
            roomList.RefreshList(response.Response, true);
        }
    }

    public void ShowAvailableMatches()
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetErrorText("");
        StartCoroutine(NetworkGateway.AvailableMatch(availableMatchesCallback));
    }

    private void availableMatchesCallback(ResponseContainer<List<MatchResume>> response)
    {
        roomList.ClearList();
        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            return;
        }

        if (roomList != null)
        {
            roomList.RefreshList(response.Response, false);
        }
    }

    private void SendTurn0(MatchResume match, string playerName, bool host)
    {
        CanvasItems.Instance.SetInfoText("Setting up the match...");
        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetButtonsEnabled(false);
        if (host)
        {
            StartCoroutine(NetworkGateway.SendTurn(matchCreatorHelper.GenerateTurn0(match, playerName, host), SendTurn0AsHostCallback));
        }
        else
        {
            StartCoroutine(NetworkGateway.SendTurn(matchCreatorHelper.GenerateTurn0(match, playerName, host), SendTurn0AsGuestCallback));
        }
    }

    private void SendTurn0AsHostCallback(ResponseContainer<TurnResume> response)
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(true);
        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            return;
        }

        MatchContainer matchContainer = MatchContainer.Instance;
        matchContainer.match.Turns?.Add(response.Response.turn);

        ChangeScene.Instance.GoToScene(mainMenuSceneName);
    }

    private void SendTurn0AsGuestCallback(ResponseContainer<TurnResume> response)
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(true);
        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            return;
        }

        MatchContainer matchContainer = MatchContainer.Instance;
        matchContainer.match.Turns?.Add(response.Response.turn);

        roomList.DestroyElem(response.Response.turn.matchId);
        CanvasItems.Instance.SetInfoText("Match joined! You can access it in your match list");
    }

    public void CreateMatch()
    {
        CanvasItems.Instance.SetInfoText("Creating the match...");
        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetButtonsEnabled(false);
        MatchResume match = matchCreatorHelper.CreateMatch();
        if (match != null)
        {
            StartCoroutine(NetworkGateway.CreateMatch(match, createMatchCallback));
        }
    }

    private void createMatchCallback(ResponseContainer<MatchResume> response)
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(true);
        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            return;
        }
        if (response.Response.Id == null || response.Response.Id.Length == 0)
        {
            CanvasItems.Instance.SetErrorText("Unable to create the requested match");
            return;
        }

        SendTurn0(response.Response, PrefsManager.Instance.GetUsername(), true);
    }

    public void JoinMatch(string matchId, Button button)
    {
        CanvasItems.Instance.AddButton(button);
        JoinMatch(matchId);
    }

    public void JoinMatch(string matchId)
    {
        CanvasItems.Instance.SetInfoText("Joining match...");
        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetButtonsEnabled(false);
        StartCoroutine(NetworkGateway.JoinMatch(matchId, joinMatchCallback));
    }

    private void joinMatchCallback(ResponseContainer<MatchResume> response)
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(true);
        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            return;
        }
        if (response.Response.Id == null || response.Response.Id.Length == 0)//Non pu� mai succedere
        {
            CanvasItems.Instance.SetErrorText("Unable to join the requested match");
            return;
        }

        SendTurn0(response.Response, PrefsManager.Instance.GetUsername(), false);
    }

    public void EnterMatch(string matchId, bool history)
    {
        CanvasItems.Instance.SetInfoText("Retrieving match info...");
        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetButtonsEnabled(false);
        if (history)
        {
            StartCoroutine(NetworkGateway.GetMatch(matchId, enterMatchWithHistoryCallback));
        }
        else
        {
            StartCoroutine(NetworkGateway.GetMatch(matchId, enterMatchCallback));
        }
    }

    private void enterMatchCallback(ResponseContainer<Match> response)
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(true);
        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            return;
        }
        if (response.Response.Id == null || response.Response.Id.Length == 0)//Non pu� mai succedere
        {
            CanvasItems.Instance.SetErrorText("Unable to find the requested match");
            return;
        }

        if (response.Response.PlayerGuest == null || response.Response.PlayerGuest.Nickname == null)
        {
            CanvasItems.Instance.SetErrorText("Wait for the guest to join");
            return;
        }

        MatchContainer matchContainer = MatchContainer.Instance;
        matchContainer.ResetContainer();
        matchContainer.match = response.Response;
        matchContainer.player = new Player();
        matchContainer.player.Nickname = PrefsManager.Instance.GetUsername();
        matchContainer.history = false;

        //GetTurnsAndEnterMatch(response.Response.Id);
        EnterCurrentScene();

    }

    private void enterMatchWithHistoryCallback(ResponseContainer<Match> response)
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(true);
        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            return;
        }
        if (response.Response.Id == null || response.Response.Id.Length == 0)//Non pu� mai succedere
        {
            CanvasItems.Instance.SetErrorText("Unable to find the requested match");
            return;
        }

        if (response.Response.PlayerGuest == null || response.Response.PlayerGuest.Nickname == null)
        {
            CanvasItems.Instance.SetErrorText("Wait for the guest to join");
            return;
        }

        MatchContainer matchContainer = MatchContainer.Instance;
        matchContainer.ResetContainer();
        matchContainer.match = response.Response;
        matchContainer.player = new Player();
        matchContainer.player.Nickname = PrefsManager.Instance.GetUsername();
        matchContainer.history = true;

        //GetTurnsAndEnterMatch(response.Response.Id);
        EnterCurrentScene();

    }

    private void GetTurnsAndEnterMatch(string matchId)
    {
        CanvasItems.Instance.SetInfoText("Retrieving match updates...");
        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetButtonsEnabled(false);
        StartCoroutine(NetworkGateway.InGameGetUpdates(matchId, 0, getTurnsAndEnterMatchCallback));
    }

    private void getTurnsAndEnterMatchCallback(ResponseContainer<List<Turn>> response)
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(true);
        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            return;
        }

        MatchContainer matchContainer = MatchContainer.Instance;
        matchContainer.match.Turns = response.Response;

        EnterCurrentScene();

    }

    private void EnterCurrentScene()
    {
        MatchContainer matchContainer = MatchContainer.Instance;
        switch (matchContainer.match.Map.Name)
        {
            case "Arena1":
                ChangeScene.Instance.GoToScene(world1Name);
                break;
            case "Arena2":
                ChangeScene.Instance.GoToScene(world2Name);
                break;
            case "Arena3":
                ChangeScene.Instance.GoToScene(world3Name);
                break;
        }
    }

}
