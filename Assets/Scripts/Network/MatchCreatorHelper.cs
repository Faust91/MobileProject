using Assets.Scripts.Network.NetworkModel;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchCreatorHelper : MonoBehaviour
{
    private const int HOURS_6 = 6 * 60 * 60;
    private const int HOURS_12 = 12 * 60 * 60;
    private const int HOURS_24 = 24 * 60 * 60;

    [SerializeField] private Text matchNameText;

    [SerializeField] private Toggle arena1Toggle;
    [SerializeField] private Toggle arena2Toggle;
    [SerializeField] private Toggle arena3Toggle;

    [SerializeField] private Toggle team1Toggle;
    [SerializeField] private Toggle team2Toggle;

    [SerializeField] private Toggle time6Toggle;
    [SerializeField] private Toggle time12Toggle;
    [SerializeField] private Toggle time24Toggle;

    private PlayerClient playerClient;

    private void Awake()
    {
        playerClient = FindObjectOfType<PlayerClient>();
    }

    public MatchResume CreateMatch()
    {
        MatchResume match = new MatchResume();

        match.PlayerHost = PrefsManager.Instance.GetUsername();

        if (match.PlayerHost == null || match.PlayerHost.Length == 0)
        {
            playerClient.Logout();
            return null;
        }

        match.MapName = GetMapName();
        match.MaxTimePlayers = GetMatchTime();
        match.PlayerHostArmy = GetPlayerTeam();
        match.MatchName = matchNameText.text;

        return match;
    }

    private string GetMapName()
    {
        if (arena1Toggle.isOn)
            return "Arena1";
        if (arena2Toggle.isOn)
            return "Arena2";
        return "Arena3";
    }

    private int GetMatchTime()
    {
        if (time6Toggle.isOn)
            return HOURS_6;
        if (time12Toggle.isOn)
            return HOURS_12;
        return HOURS_24;
    }

    private Army GetPlayerTeam()
    {
        if (team1Toggle.isOn)
            return Army.Humans;
        return Army.Monsters;
    }

    public Turn GenerateTurn0(MatchResume match, string playerName, bool host)
    {
        Turn turn0 = new Turn(playerName, match, 0);
        turn0.SetBegin(DateTime.UtcNow.ToUniversalTime());
        turn0.End = turn0.Begin;

        switch (match.MapName)
        {
            case "Arena1": // 10 5
                if (host)
                {
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Commander", 4, 5));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Archer", 2, 8));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Archer", 1, 1));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Knight", 4, 2));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Pikeman", 7, 8));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Pikeman", 6, 4));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Infantryman", 2, 6));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Infantryman", 1, 4));

                }
                else
                {
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Commander", 15, 5));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Archer", 17, 8));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Archer", 18, 1));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Knight", 15, 2));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Pikeman", 12, 8));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Pikeman", 13, 4));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Infantryman", 17, 6));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Infantryman", 18, 4));
                }
                break;
            case "Arena2": // 10 7
                if (host)
                {
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Commander", 2, 12));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Archer", 4, 9));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Archer", 6, 9));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Knight", 2, 2));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Knight", 6, 2));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Knight", 3, 1));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Pikeman", 5, 11));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Pikeman", 9, 10));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Infantryman", 4, 3));

                }
                else
                {
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Commander", 17, 1));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Archer", 13, 4));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Archer", 15, 4));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Knight", 13, 11));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Knight", 16, 12));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Knight", 17, 11));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Pikeman", 10, 3));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Pikeman", 14, 2));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Infantryman", 15, 10));
                }
                break;
            case "Arena3": // 10 6
                if (host)
                {
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Commander", 3, 9));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Commander", 5, 8));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Archer", 7, 9));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Knight", 15, 10));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Knight", 16, 8));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Pikeman", 3, 6));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Infantryman", 12, 8));

                }
                else
                {
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Commander", 14, 3));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Commander", 16, 2));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Archer", 12, 2));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Knight", 4, 1));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Knight", 3, 3));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Pikeman", 16, 5));
                    turn0.Moves.Add(GenerateSpawnMove(turn0, "Infantryman", 7, 3));
                }
                break;
            default:
                break;
        }

        return turn0;
    }

    private Move GenerateSpawnMove(Turn turn, string unitName, int posX, int posY)
    {
        Unit unit0 = new Unit();
        unit0.Name = unitName;
        return new Move(turn, Assets.Scripts.Network.NetworkModel.Action.Spawn, unit0, posX, posY, posX, posY);
    }
}
