using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGamePoller : MonoBehaviour
{
    private InGameClient inGameClient;

    private Coroutine othersTurnCoroutineHandle;

    private const int SECONDS_TO_WAIT_FOR = 5;

    private Action<int> turnsPollingCallback = null;

    private void Awake()
    {
        inGameClient = GetComponent<InGameClient>();
    }

    public void StartPollingOthersTurn(Action<int> callback)
    {
        StopPollingOthersTurn();
        turnsPollingCallback = callback;
        othersTurnCoroutineHandle = StartCoroutine(OthersTurnCoroutine());
    }

    private IEnumerator OthersTurnCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(SECONDS_TO_WAIT_FOR);
            inGameClient.GetTurns(MatchContainer.Instance.match.Id, false, turnsPollingCallback);
        }
    }

    public void StopPollingOthersTurn()
    {
        if (othersTurnCoroutineHandle != null)
        {
            StopCoroutine(othersTurnCoroutineHandle);
        }
    }

    private void StopEveryPolling()
    {
        StopPollingOthersTurn();
    }

    private void OnDestroy()
    {
        StopEveryPolling();
    }
}
