using Assets.Scripts.Network.NetworkModel;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameClient : MonoBehaviour
{
    Action<int> getTurnsResponseCallback = null;

    private static DateTime TIME_ZERO = new DateTime();

    public void GetTurns(string matchId)
    {
        GetTurns(matchId, true, null);
    }

    public void GetTurns(string matchId, bool replaceAll, Action<int> responseCallback)
    {
        getTurnsResponseCallback = responseCallback;
        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(false);
        if (replaceAll)
        {
            StartCoroutine(NetworkGateway.InGameGetUpdates(matchId, 0, getTurnsAndReplaceTurnsCallback));
        }
        else
        {
            StartCoroutine(NetworkGateway.InGameGetUpdates(matchId, 0, getTurnsAndUpdateTurnsCallback));
        }
    }

    private void getTurnsAndReplaceTurnsCallback(ResponseContainer<List<Turn>> response)
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(true);
        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            return;
        }

        MatchContainer matchContainer = MatchContainer.Instance;
        matchContainer.match.Turns = response.Response;
    }

    private void getTurnsAndUpdateTurnsCallback(ResponseContainer<List<Turn>> response)
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(true);
        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            return;
        }

        MatchContainer matchContainer = MatchContainer.Instance;
        if (matchContainer.match.Turns.Count < response.Response.Count
            || (!response.Response[response.Response.Count - 1].GetEnd().Equals(TIME_ZERO)) && !matchContainer.gameOver)
        {
            int previousCount = matchContainer.match.Turns.Count;
            matchContainer.match.Turns = response.Response;

            getTurnsResponseCallback?.Invoke(previousCount);
        }

    }

    public void SendTurn(Turn turn)
    {
        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetInfoText("Sending turn to the server...");
        CanvasItems.Instance.SetButtonsEnabled(false);
        StartCoroutine(NetworkGateway.SendTurn(turn, SendTurnCallback));
    }

    private void SendTurnCallback(ResponseContainer<TurnResume> response)
    {
        CanvasItems.Instance.SetInfoText("");
        CanvasItems.Instance.SetButtonsEnabled(true);
        if (response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Nothing found");
            return;
        }

        //MatchContainer matchContainer = MatchContainer.Instance;
        //matchContainer.match.Turns.Add(response.Response.turn);
        GetTurns(response.Response.turn.matchId, true, null);

    }
}
