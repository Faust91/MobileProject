using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Network.NetworkModel;

public class PlayerClient : MonoBehaviour
{
    [SerializeField] private InputField nicknameInputField;
    [SerializeField] private InputField passwordInputField;
    [SerializeField] private string mainMenuSceneName = "MainMenuScene";
    [SerializeField] private string loginSceneName = "LoginScene";
    [SerializeField] private string logoutSceneName = "LoggingMenuScene";

    public void Logout()
    {
        PrefsManager.Instance.DeleteUsernameAndToken();
        ChangeScene.Instance.GoToScene(logoutSceneName);
    }

    public void RegisterUser()
    {
        string username = nicknameInputField.text;
        string password = passwordInputField.text;

        if (username == null || username.Length == 0 || password == null || password.Length == 0)
        {
            CanvasItems.Instance.SetErrorText("Fill all fields");
            return;
        }

        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetButtonsEnabled(false);

        StartCoroutine(NetworkGateway.SignIn(new AuthenticateModel(username, password), registerUserCallback));
    }

    public void LoginUser()
    {
        string username = nicknameInputField.text;
        string password = passwordInputField.text;

        if(username == null || username.Length == 0 || password == null || password.Length == 0)
        {
            CanvasItems.Instance.SetErrorText("Fill all fields");
            return;
        }

        CanvasItems.Instance.SetErrorText("");
        CanvasItems.Instance.SetButtonsEnabled(true);

        StartCoroutine(NetworkGateway.Login(new AuthenticateModel(username, password), loginUserCallback));
    }

    private void registerUserCallback(ResponseContainer<Player> response)
    {
        CanvasItems.Instance.SetButtonsEnabled(true);
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }

        ChangeScene.Instance.GoToScene(loginSceneName);
    }

    private void loginUserCallback(ResponseContainer<AuthenticatedWithToken> response)
    {
        CanvasItems.Instance.SetButtonsEnabled(true);
        string error = response.Error;
        if (error != null && error.Length > 0)
        {
            CanvasItems.Instance.SetErrorText(error);
            return;
        }
        if (response.Response == null)
        {
            CanvasItems.Instance.SetErrorText("Unknown error");
            return;
        }

        PrefsManager.Instance.SaveUsernameAndToken(response.Response.Nickname, response.Response.Token);
        ChangeScene.Instance.GoToScene(mainMenuSceneName);
    }

}
