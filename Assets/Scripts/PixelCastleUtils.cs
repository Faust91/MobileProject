using Assets.Scripts.Network.NetworkModel;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelCastleUtils
{
    public static bool AmIHost()
    {
        string myUsername = PrefsManager.Instance.GetUsername();
        return myUsername.Equals(MatchContainer.Instance.match.PlayerHost.Nickname);
    }

    public static bool IsItMyTeam(TeamEnum team, TeamsManager teamsManager)
    {
        return team == teamsManager.Team;
    }

    public static int ArmyToInt(Army army)
    {
        return (army == Army.Humans ? 1 : 2);
    }

    public static TeamEnum ArmyToTeam(Army army)
    {
        return (army == Army.Humans ? TeamEnum.Team1 : TeamEnum.Team2);
    }

    public static int GetHostArmy()
    {
        return ArmyToInt(MatchContainer.Instance.match.PlayerHostArmy);
    }

    public static int GetGuestArmy()
    {
        return ArmyToInt(MatchContainer.Instance.match.PlayerGuestArmy);
    }

    public static TeamEnum GetHostTeam()
    {
        return ArmyToTeam(MatchContainer.Instance.match.PlayerHostArmy);
    }

    public static TeamEnum GetGuestTeam()
    {
        return ArmyToTeam(MatchContainer.Instance.match.PlayerGuestArmy);
    }

    public static int GetMyArmy()
    {
        return GetArmyByHost(AmIHost());
    }

    public static int GetOpponentsArmy()
    {
        return GetArmyByHost(!AmIHost());
    }

    public static TeamEnum GetMyTeam()
    {
        return GetTeamByHost(AmIHost());
    }

    public static TeamEnum GetOpponentsTeam()
    {
        return GetTeamByHost(!AmIHost());
    }

    // PRIVATE

    private static int GetArmyByHost(bool host)
    {
        return host ? GetHostArmy() : GetGuestArmy();
    }

    private static TeamEnum GetTeamByHost(bool host)
    {
        return host ? GetHostTeam() : GetGuestTeam();
    }
}
