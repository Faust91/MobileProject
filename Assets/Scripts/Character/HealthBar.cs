using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Slider slider;
    [SerializeField] private float speed = 1.0f;
    [SerializeField] private float epsilon = 0.01f;

    private Coroutine slideCoroutine;

    public void DestroyMe()
    {
        if (slideCoroutine != null)
        {
            StopCoroutine(slideCoroutine);
        }
        Destroy(gameObject);
    }

    public void SetHealth(float amount)
    {
        slider.value = amount;
    }

    public void MoveHealthToValue(float amount)
    {
        if (slideCoroutine != null)
        {
            StopCoroutine(slideCoroutine);
        }
        slideCoroutine = StartCoroutine(SlideCoroutine(amount));
    }

    private IEnumerator SlideCoroutine(float targetHealth)
    {
        while (Mathf.Abs(slider.value - targetHealth) > epsilon)
        {
            slider.value = Mathf.MoveTowards(slider.value, targetHealth, speed * Time.deltaTime);
            yield return null;

        }
        slider.value = targetHealth;
    }

}
