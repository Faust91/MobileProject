using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXContainer : MonoBehaviour
{
    public AudioClip walkSFX;
    public AudioClip attackSFX;
    public AudioClip deathSFX;
    private static SFXContainer _instance;
    public static SFXContainer Instance { get { return _instance; }  }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            _instance.InitSingleton();
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void InitSingleton()
    {
        // PUT OTHER STUFF HERE...
    }
    // SINGLETON STUFF ENDS HERE 
}
