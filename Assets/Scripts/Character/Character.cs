using Assets.Scripts.Network.NetworkModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public static Vector3 VectorOffset = new Vector3(0.5f, 0.5f, 0.0f);

    private GridController grid;
    private GridInfo gridInfo;
    private MarkerDrawer brush;
    [SerializeField] private CharacterStats defaultStats;
    private CharacterStats stats;
    [SerializeField] private TeamEnum team;
    private TeamsManager teamManager;
    private TurnManager turn;
    private bool busy = false;
    private List<Cell> cells = new List<Cell>();
    private List<Cell> path = null;
    private float movementSpeed = 3.0f;
    private float deltaPosition = 0.01f;
    [SerializeField] private bool dead = false;
    private Character enemyToAttack = null;
    [SerializeField] private GameObject spriteChild;
    private Animator animator;
    private SpriteRenderer sr;
    [Header("Status")]
    [SerializeField] private bool canMove = true;
    [SerializeField] private bool canAttack = true;
    [SerializeField] private bool hasMoved = false;
    [SerializeField] private bool hasAttacked = false;
    private HealthBar healthBar;
    private MatchContainer matchContainer;
    private AudioSource audioSource;
    private SFXContainer sfxContainer;

    public bool IsKing()
    {
        return stats.CharacterType == CharacterTypeEnum.KING;
    }

    public bool CanMoveOrAttack()
    {
        return ((canMove && !hasMoved) || (canAttack && !hasAttacked));
    }

    public void ResetMoves()
    {
        hasMoved = false;
        hasAttacked = false;
        UpdateMoves();
    }

    public void LoadStatus(CharacterStats _stats, bool _hasMoved, bool _hasAttacked, TeamEnum _team)
    {
        // TODO: not yet implemented...
    }

    public CharacterStats GetStats()
    {
        return stats;
    }

    public bool IsDead()
    {
        return dead;
    }

    private void Awake()
    {
        matchContainer = FindObjectOfType<MatchContainer>();
        if (matchContainer == null)
        {
            Debug.LogError("No match container found!");
        }

        stats = CharacterStats.CreateInstance<CharacterStats>();
        CharacterStats.CopyStats(defaultStats, stats);
        grid = FindObjectOfType<GridController>();
        gridInfo = FindObjectOfType<GridInfo>();
        brush = FindObjectOfType<MarkerDrawer>();
        teamManager = FindObjectOfType<TeamsManager>();
        turn = FindObjectOfType<TurnManager>();
        animator = spriteChild.GetComponent<Animator>();
        sr = spriteChild.GetComponent<SpriteRenderer>();
        healthBar = GetComponentInChildren<HealthBar>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        sfxContainer = SFXContainer.Instance;
        Vector3 centerOfGrid = new Vector3(gridInfo.HalfWidth, gridInfo.HalfHeight);
        LookAtTheRightSide(centerOfGrid);
        healthBar.SetHealth(stats.Health / defaultStats.Health);
    }

    private void lookRight()
    {
        sr.flipX = false;
    }

    private void lookLeft()
    {
        sr.flipX = true;
    }

    public void DestroyMe()
    {
        //sr.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        sr.enabled = false;
        healthBar.DestroyMe();
        //Destroy(gameObject);
    }

    public void ControlMe()
    {
        teamManager.ControlCharacter(this);
    }

    //public void UpdateAndShowMoves()
    //{
    //    UpdateMoves();
    //    ShowMoves();
    //}

    public void UpdateMoves()
    {
        cells = grid.GetReachableCells(transform.position, this, !hasMoved, !hasAttacked);
        canMove = cells.Find(x => x.Type == Cell.CellType.FLOOR) != null;
        canAttack = cells.Find(x => x.Type == Cell.CellType.ENEMY) != null;
    }

    public void ShowMoves()
    {
        brush.MarkInteractableCells(cells, true);
    }

    public TeamEnum Team
    {
        get { return team; }
        set { team = value; }
    }

    public bool SameTeam(Character other)
    {
        return (other != null && this.team == other.team);
    }

    private void PrepareToAttack(Character enemy)
    {
        enemyToAttack = enemy;
    }

    public void ListenToClick(Vector3Int clickPosition)
    {
        ListenToClick(clickPosition, false);
    }

    public void ListenToClick(Vector3Int clickPosition, bool remote)
    {
        Cell cell = cells.Find(x => x.Position.Equals(clickPosition));
        if (cell != null)
        {
            if (cell.Type == Cell.CellType.ALLY)
            {
                Character clickedCharacter = teamManager.FindLivingCharacter(clickPosition);
                LeaveCharacter();
                teamManager.ControlCharacter(clickedCharacter);
                clickedCharacter.ShowMoves();
            }
            else if (cell.Type == Cell.CellType.FLOOR)
            {
                path = FindPathToReachTarget(cell);
                LeaveCharacter();
                busy = true;
                turn.SetPause(true);
                hasMoved = true;
                animator.SetBool("Moving", true);
                SFXWalk();
                if (!remote)
                {
                    Move newMove = new Move();
                    newMove.Action = Action.Moving;
                    newMove.PlayerId = PrefsManager.Instance.GetUsername();
                    newMove.UnitName = stats.UnitType.ToString();
                    Vector3 intPosV3 = transform.position - VectorOffset;
                    newMove.DepartureX = (int)intPosV3.x;
                    newMove.DepartureY = (int)intPosV3.y;
                    newMove.ArrivalX = clickPosition.x;
                    newMove.ArrivalY = clickPosition.y;
                    matchContainer.match.Turns[matchContainer.lastTurnToPlay].Moves.Add(newMove);
                }
            }
            else if (cell.Type == Cell.CellType.ENEMY)
            {
                LookAtTheRightSide(cell.Position);
                teamManager.LeaveCharacter(this);
                brush.ClearMarkers();
                busy = true;
                turn.SetPause(true);
                hasAttacked = true;
                enemyToAttack = teamManager.FindLivingCharacter(cell.Position);

                if (!remote)
                {
                    Move newMove = new Move();
                    newMove.Action = Action.Attack;
                    newMove.PlayerId = PrefsManager.Instance.GetUsername();
                    newMove.UnitName = stats.UnitType.ToString();
                    Vector3 intPosV3 = transform.position - VectorOffset;
                    newMove.DepartureX = (int)intPosV3.x;
                    newMove.DepartureY = (int)intPosV3.y;
                    newMove.ArrivalX = (int)intPosV3.x;
                    newMove.ArrivalY = (int)intPosV3.y;
                    newMove.AttackX = clickPosition.x;
                    newMove.AttackY = clickPosition.y;
                    matchContainer.match.Turns[matchContainer.lastTurnToPlay].Moves.Add(newMove);
                }
            }
            else
            {
                LeaveCharacter();
                if (remote)
                {
                    turn.NextRemote();
                }
                else
                {
                    turn.Next();
                }
            }
        }
        else
        {
            LeaveCharacter();
            if (remote)
            {
                turn.NextRemote();
            }
            else
            {
                turn.Next();
            }
        }
    }

    private void LookAtTheRightSide(Vector3 target)
    {
        if (target.x > transform.position.x) // BRUTALITY
        {
            lookRight();
        }
        else if (target.x < transform.position.x) // BRUTALITY
        {
            lookLeft();
        }
    }


    private void LeaveCharacter()
    {
        teamManager.LeaveCharacter(this);
        brush.ClearMarkers();
    }

    private void Update()
    {
        if (!dead)
        {
            if (busy)
            {
                if (path != null && path.Count > 0)
                {
                    Vector3 currDest = grid.GetWorldPosition(path[0].Position) + VectorOffset;
                    if (Vector3.Distance(transform.position, currDest) > deltaPosition)
                    {
                        LookAtTheRightSide(currDest);
                        transform.position = Vector3.MoveTowards(transform.position, currDest, movementSpeed * Time.deltaTime);
                    }
                    else
                    {
                        transform.position = currDest;
                        path.RemoveAt(0);
                    }
                }
                else
                {
                    busy = false;
                    turn.SetPause(false);
                    animator.SetBool("Moving", false);
                    SFXIdle();
                    if (enemyToAttack != null)
                    {
                        animator.SetTrigger("Attack");
                        SFXAttack();
                        enemyToAttack.TakeDamage(stats, true, true);
                        enemyToAttack = null;
                    }
                    else
                    {
                        turn.Next();
                    }
                }
            }
        }
    }

    public void TakeDamage(CharacterStats attackerStats, bool animated, bool resume)
    {
        float amount = CharacterStats.GetDamageAmount(attackerStats, stats);

        stats.Health -= Mathf.Clamp(amount, 0.0f, stats.Health);
        if (animated)
        {
            healthBar.MoveHealthToValue(stats.Health / defaultStats.Health);
        }
        else
        {
            healthBar.SetHealth(stats.Health / defaultStats.Health);
        }

        if (stats.Health <= 0)
        {
            dead = true;
            if (animated)
            {
                animator.SetTrigger("IsDead");
                SFXDeath();
            }
            else
            {
                DestroyMe();
            }
        }
        else
        {
            if (animated)
            {
                animator.SetTrigger("Hit");
            }
        }
        if (resume)
        {
            turn.Next();
        }
    }

    private List<Cell> FindPathToReachTarget(Cell target)
    {
        List<Cell> retList = new List<Cell>();
        int distanceFromTarget = target.Distance;
        if (target.Type == Cell.CellType.FLOOR)
        {
            retList.Add(target);
        }
        distanceFromTarget--;
        while (distanceFromTarget > 0)
        {
            foreach (Cell cell in cells.FindAll(x => ((x.Distance == distanceFromTarget) && x.Type == Cell.CellType.FLOOR)))
            {
                if ((retList.Count > 0 && cell.IsAdjacent(retList[retList.Count - 1])) || cell.IsAdjacent(target))
                {
                    retList.Add(cell);
                    break;
                }
            }
            distanceFromTarget--;
        }
        retList.Reverse();
        return retList;
    }

    private void SFXAttack()
    {
        audioSource.Stop();
        audioSource.clip = sfxContainer.attackSFX;
        audioSource.loop = false;
        audioSource.Play();
    }

    private void SFXWalk()
    {
        audioSource.Stop();
        audioSource.clip = sfxContainer.walkSFX;
        audioSource.loop = true;
        audioSource.Play();
    }

    private void SFXDeath()
    {
        audioSource.Stop();
        audioSource.clip = sfxContainer.deathSFX;
        audioSource.loop = false;
        audioSource.Play();
    }


    private void SFXIdle()
    {
        audioSource.Stop();
    }
}
