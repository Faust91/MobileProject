using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TeamEnum
{
    None, Team1, Team2, Both
}

public class TeamsManager : MonoBehaviour
{
    [SerializeField] private TeamEnum team;
    private GridController grid;
    [SerializeField] private bool controlBothTeams = false;
    private List<Character> characters;
    private InputClick inputClick;

    [Header("Character Prefabs")]
    [SerializeField] public GameObject ArcherPrefab;
    [SerializeField] public GameObject CavalierPrefab;
    [SerializeField] public GameObject DeathKnightPrefab;
    [SerializeField] public GameObject GhostPrefab;
    [SerializeField] public GameObject PaladinPrefab;
    [SerializeField] public GameObject PikemanPrefab;
    [SerializeField] public GameObject SkeletonPrefab;
    [SerializeField] public GameObject SwordsmanPrefab;
    [SerializeField] public GameObject VampirePrefab;
    [SerializeField] public GameObject ZombiePrefab;

    [Header("UI")]
    [SerializeField] private GameObject myTurnDescription;
    [SerializeField] private GameObject opponentsTurnDescription;

    public bool ControlBothTeams
    {
        get { return controlBothTeams; }
        set { controlBothTeams = value; }
    }

    private void DisableTurnDescriptions()
    {
        myTurnDescription.SetActive(false);
        opponentsTurnDescription.SetActive(false);
    }

    public void UpdateTurnDescriptions(TeamEnum currentTeam)
    {
        if (currentTeam == team)
        {
            myTurnDescription.SetActive(true);
            opponentsTurnDescription.SetActive(false);
        }
        else
        {
            myTurnDescription.SetActive(false);
            opponentsTurnDescription.SetActive(true);
        }
    }

    private void Awake()
    {
        grid = FindObjectOfType<GridController>();
        inputClick = FindObjectOfType<InputClick>();
    }

    private void Start()
    {
        DisableTurnDescriptions();
    }

    public void AfterCharactersSpawn()
    {
        LoadCharacters();
    }

    public TeamEnum Team
    {
        get { return team; }
        set { team = value; }
    }

    public void ControlCharacter(Character character)
    {
        //inputClick.ClearOnTileClick();
        inputClick.OnTileClick += character.ListenToClick;
    }

    public void LeaveCharacter(Character character)
    {
        //inputClick.ClearOnTileClick();
        inputClick.OnTileClick -= character.ListenToClick;
    }

    public List<Character> FindLivingCharacters()
    {
        return FindCharacters().FindAll(x => !x.IsDead());
    }

    private List<Character> FindCharacters()
    {
        return characters;
    }

    private void LoadCharacters()
    {
        Character[] charactersArray = FindObjectsOfType<Character>();
        if (charactersArray != null)
        {
            characters = new List<Character>(charactersArray);
        }
        else
        {
            characters = new List<Character>();
        }
    }

    public Character FindLivingCharacter(Vector3Int position)
    {
        return FindCharacter(position, FindLivingCharacters());
    }

    private Character FindCharacter(Vector3Int position, List<Character> characters)
    {
        foreach (Character character in characters)
        {
            if (grid.GetCellPosition(character.transform.position).Equals(position))
            {
                return character;
            }
        }
        return null;
    }

    public void ResetTeamMoves(TeamEnum _team)
    {
        foreach (Character character in FindLivingCharacters().FindAll(x => x.Team == _team))
        {
            character.ResetMoves();
        }
    }

    public void UpdateMoves()
    {
        foreach (Character character in FindLivingCharacters())
        {
            character.UpdateMoves();
        }
    }

    public bool CanTeamMove(TeamEnum _team)
    {
        foreach (Character character in FindLivingCharacters().FindAll(x => x.Team == _team))
        {
            if (character.CanMoveOrAttack())
            {
                return true;
            }
        }
        return false;
    }
}
