using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefsManager : MonoBehaviour
{
    // SINGLETON STUFF STARTS HERE 
    private static PrefsManager _instance;
    public static PrefsManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            _instance.InitSingleton();
        }
        DontDestroyOnLoad(this.gameObject);
    }
    private void InitSingleton()
    {
        // PUT OTHER STUFF HERE...
    }
    // SINGLETON STUFF ENDS HERE 

    [SerializeField] private bool cachedPrefs = true;

    private string username;
    private string token;

    public string GetUsername()
    {
        if (cachedPrefs)
        {
            if(username == null || username.Length == 0)
            {
                username = GetUsernameNoCache();
            }
            return username;
        }
        else
        {
            return GetUsernameNoCache();
        }
        
    }

    private string GetUsernameNoCache()
    {
        return PlayerPrefs.GetString("username");
    }

    public string GetToken()
    {
        if (cachedPrefs)
        {
            if (token == null || token.Length == 0)
            {
                token = GetTokenNoCache();
            }
            return token;
        }
        else
        {
            return GetTokenNoCache();
        }
    }

    private string GetTokenNoCache()
    {
        return PlayerPrefs.GetString("token");
    }

    public void SaveUsernameAndToken(string username, string token)
    {
        this.username = username;
        this.token = token;
        PlayerPrefs.SetString("username", username);
        PlayerPrefs.SetString("token", token);
        PlayerPrefs.Save();
    }

    public void DeleteUsernameAndToken()
    {
        this.username = null;
        this.token = null;
        PlayerPrefs.DeleteKey("username");
        PlayerPrefs.DeleteKey("token");
        PlayerPrefs.Save();
    }

}
