using Assets.Scripts.Network.NetworkModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private TeamsManager teamManager;
    private GameOverScreen gameOverScreen;
    private TurnManager turnManager;

    private void Awake()
    {
        teamManager = GetComponent<TeamsManager>();
        gameOverScreen = FindObjectOfType<GameOverScreen>();
        turnManager = GetComponent<TurnManager>();
    }

    private void Start()
    {
        CanvasItems.Instance.PlayBattleMusic();
    }

    public TeamEnum CheckGameOver()
    {
        TeamEnum winner = TeamEnum.None;

        double remainingMinutesForCurrentTeam = MatchContainer.Instance.GetRemainingMinutes(PixelCastleUtils.IsItMyTeam(turnManager.Team, teamManager));
        double remainingMinutesForOtherTeam = MatchContainer.Instance.GetRemainingMinutes(!PixelCastleUtils.IsItMyTeam(turnManager.Team, teamManager));

        if (remainingMinutesForOtherTeam <= 0)
        {
            winner = turnManager.Team;
        }
        else if (remainingMinutesForCurrentTeam <= 0)
        {
            winner = turnManager.Team == TeamEnum.Team1 ? TeamEnum.Team2 : TeamEnum.Team1;
        }

        if (winner == TeamEnum.None)
        {

            List<Character> characters = teamManager.FindLivingCharacters();

            bool king1Present = false;
            bool king2Present = false;

            if (characters != null)
            {
                foreach (Character character in characters)
                {
                    if (character.IsKing())
                    {
                        if (character.Team == TeamEnum.Team1)
                        {
                            king1Present = true;
                        }
                        else if (character.Team == TeamEnum.Team2)
                        {
                            king2Present = true;
                        }
                    }
                }
            }

            if (!king1Present && !king2Present)
            {
                winner = TeamEnum.Both;
            }
            else if (!king1Present)
            {
                winner = TeamEnum.Team2;
            }
            else if (!king2Present)
            {
                winner = TeamEnum.Team1;
            }

        }

        if (winner != TeamEnum.None)
        {
            if (winner == teamManager.Team)
            {
                CanvasItems.Instance.PlayVictory();
            }
            else
            {
                CanvasItems.Instance.PlayDefeat();
            }

            gameOverScreen.ShowGameOverScreen(winner);
        }

        return winner;
    }

    private void OnDestroy()
    {
        //CanvasItems.Instance.StopMusic();
        CanvasItems.Instance.PlayMainMenuMusic();
    }


}
