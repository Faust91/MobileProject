using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkSettings : MonoBehaviour
{
    //private string BASE_URL_LOCAL = "https://localhost:44357/api/";
    //private string BASE_URL_REMOTE = "http://51.140.137.53:5100/api/";

    //private string URL_PREFIX = "https://";
    private string URL_BETWEEN = ":";
    private string URL_SUFFIX = "/api/";

    [SerializeField] private string localServerIp = "https://localhost";
    [SerializeField] private string localServerPort = "44357";

    [SerializeField] private string remoteServerIp = "http://40.67.235.81";
    [SerializeField] private string remoteServerPort = "";

    // SINGLETON STUFF STARTS HERE 
    private static NetworkSettings _instance;
    public static NetworkSettings Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            _instance.InitSingleton();
        }
        DontDestroyOnLoad(this.gameObject);
    }
    private void InitSingleton()
    {
        // PUT OTHER STUFF HERE...
        InitNetworking();
    }
    // SINGLETON STUFF ENDS HERE 

    [SerializeField] private bool remoteServer = true;

    private void InitNetworking()
    {
        if (remoteServer)
        {
            NetworkGateway.SetServer(remoteServerIp
                + ((remoteServerPort == null || remoteServerPort.Length <= 0) ? "" : (URL_BETWEEN + remoteServerPort))
                + URL_SUFFIX);
        }
        else
        {
            NetworkGateway.SetServer(localServerIp
                + ((localServerPort == null || localServerPort.Length <= 0) ? "" : (URL_BETWEEN + localServerPort))
                + URL_SUFFIX);
        }
    }
}
