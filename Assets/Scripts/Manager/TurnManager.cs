using Assets.Scripts.Network.NetworkModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnManager : MonoBehaviour
{
    private TeamsManager teamManager;
    private InputClickListen inputClickListen;
    private GameController gameController;
    private MarkerDrawer brush;
    private TeamScreen teamScreen;
    private InGameClient inGameClient;

    [Header("Settings")]
    [SerializeField] private Button endTurnButton;
    //[SerializeField] private Text timerText;
    //[SerializeField] private int timerSeconds = 300;

    [Header("Context")]
    [SerializeField] private TeamEnum team = TeamEnum.Team1;
    //[SerializeField] private float timer;
    [SerializeField] private bool pause;
    [SerializeField] private bool remoteSimulation = false;

    private MatchContainer matchContainer;
    private List<Move> movesToApply = new List<Move>();
    private InGamePoller inGamePoller;

    private static System.DateTime TIME_ZERO = new System.DateTime();

    private int RemoteArmyToMove(Turn turn)
    {
        bool itsHost = turn.ownerId == matchContainer.match.PlayerHost.Nickname;
        int hostArmy = matchContainer.match.PlayerHostArmy == Army.Humans ? 1 : 2;
        int guestArmy = matchContainer.match.PlayerGuestArmy == Army.Humans ? 1 : 2;
        return itsHost ? hostArmy : guestArmy;
    }

    private int MyArmy()
    {
        bool itsHost = PrefsManager.Instance.GetUsername() == matchContainer.match.PlayerHost.Nickname;
        int hostArmy = matchContainer.match.PlayerHostArmy == Army.Humans ? 1 : 2;
        int guestArmy = matchContainer.match.PlayerGuestArmy == Army.Humans ? 1 : 2;
        return itsHost ? hostArmy : guestArmy;
    }

    private int CurrentArmy()
    {
        return team == TeamEnum.Team1 ? 1 : 2;
    }

    private void InstantApplyRemoteTurn(Turn turn)
    {
        foreach (Move move in turn.Moves)
        {
            int armyToMove = RemoteArmyToMove(turn);

            switch (move.Action)
            {
                case Action.Spawn:
                    GameObject prefabToSpawn = null;
                    switch (move.UnitName)
                    {
                        case "Commander":
                            if (armyToMove == 1)
                            {
                                prefabToSpawn = teamManager.PaladinPrefab;
                            }
                            else if (armyToMove == 2)
                            {
                                prefabToSpawn = teamManager.VampirePrefab;
                            }
                            break;
                        case "Archer":
                            if (armyToMove == 1)
                            {
                                prefabToSpawn = teamManager.ArcherPrefab;
                            }
                            else if (armyToMove == 2)
                            {
                                prefabToSpawn = teamManager.GhostPrefab;
                            }
                            break;
                        case "Knight":
                            if (armyToMove == 1)
                            {
                                prefabToSpawn = teamManager.SwordsmanPrefab;
                            }
                            else if (armyToMove == 2)
                            {
                                prefabToSpawn = teamManager.SkeletonPrefab;
                            }
                            break;
                        case "Pikeman":
                            if (armyToMove == 1)
                            {
                                prefabToSpawn = teamManager.PikemanPrefab;
                            }
                            else if (armyToMove == 2)
                            {
                                prefabToSpawn = teamManager.ZombiePrefab;
                            }
                            break;
                        case "Infantryman":
                            if (armyToMove == 1)
                            {
                                prefabToSpawn = teamManager.CavalierPrefab;
                            }
                            else if (armyToMove == 2)
                            {
                                prefabToSpawn = teamManager.DeathKnightPrefab;
                            }
                            break;
                        default:
                            break;
                    }
                    if (prefabToSpawn != null)
                    {
                        GameObject spawnedCharacter = Instantiate(prefabToSpawn, new Vector3(move.ArrivalX, move.ArrivalY, 0.0f) + Character.VectorOffset, Quaternion.identity);
                        spawnedCharacter.GetComponent<Character>().Team = armyToMove == 1 ? TeamEnum.Team1 : TeamEnum.Team2;
                    }
                    break;
                case Action.Attack:
                    Character attackingCharacter = teamManager.FindLivingCharacter(new Vector3Int(move.DepartureX, move.DepartureY, 0));
                    Character attackedCharacter = teamManager.FindLivingCharacter(new Vector3Int(move.AttackX, move.AttackY, 0));
                    attackedCharacter.TakeDamage(attackingCharacter.GetStats(), false, false);
                    break;
                case Action.Moving:
                    Character movingCharacter = teamManager.FindLivingCharacter(new Vector3Int(move.DepartureX, move.DepartureY, 0));
                    GameObject movingGameObject = movingCharacter.gameObject;
                    movingGameObject.transform.position = new Vector3(move.ArrivalX, move.ArrivalY, 0.0f) + Character.VectorOffset;
                    break;
            }
        }
    }

    private void MakeMove(Move move)
    {
        Character character = teamManager.FindLivingCharacter(new Vector3Int(move.DepartureX, move.DepartureY, 0));
        if (character == null)
        {
            Debug.LogError("No character found for requested move");
        }
        else if (move.Action == Action.Moving)
        {
            character.ListenToClick(new Vector3Int(move.ArrivalX, move.ArrivalY, 0), true);
        }
        else if (move.Action == Action.Attack)
        {
            character.ListenToClick(new Vector3Int(move.AttackX, move.AttackY, 0), true);
        }
    }

    public void ApplyRemoteTurn(Turn turn)
    {
        TeamEnum teamToMove;
        if (RemoteArmyToMove(turn) == 1)
        {
            teamToMove = TeamEnum.Team1;
        }
        else
        {
            teamToMove = TeamEnum.Team2;
        }
        movesToApply = new List<Move>(turn.Moves);
        remoteSimulation = true;
        SelectTeam(teamToMove, true);
    }

    public void UnpauseAndNext()
    {
        SetPause(false);
        Next();
    }
    public void SetPause(bool _pause)
    {
        pause = _pause;
    }

    public bool IsPause()
    {
        return pause;
    }

    public TeamEnum Team
    {
        get { return team; }
    }

    private void Awake()
    {
        teamManager = GetComponent<TeamsManager>();
        inputClickListen = GetComponent<InputClickListen>();
        gameController = GetComponent<GameController>();
        brush = FindObjectOfType<MarkerDrawer>();
        teamScreen = FindObjectOfType<TeamScreen>();
        inGameClient = FindObjectOfType<InGameClient>();
        inGamePoller = FindObjectOfType<InGamePoller>();
    }

    private void Start()
    {
        matchContainer = MatchContainer.Instance;
        if (matchContainer == null)
        {
            Debug.LogError("No match container found!");
        }
        teamManager.Team = MyArmy() == 1 ? TeamEnum.Team1 : TeamEnum.Team2;

        AddRemoteTurnsAndApply();
    }

    private void AddRemoteTurnsAndApply()
    {
        Match match = matchContainer.match;
        List<Turn> previousTurns = match.Turns;
        if (previousTurns.Count == 0)
        {
            // non dovrebbe mai succedere
            EndTurn(false);
            return;
        }

        if (matchContainer.lastTurnToPlay == matchContainer.match.Turns.Count - 1
            && !matchContainer.match.Turns[matchContainer.match.Turns.Count - 1].GetEnd().Equals(TIME_ZERO))
        {
            // l'ultimo turno ricevuto  ancora da giocare
            //    matchContainer.match.Turns[matchContainer.lastTurnToPlay].SetBeginToNow();
            //}
            //else
            //{
            // l'ultimo turno  stato gi giocato ed  un game over
            matchContainer.gameOver = true;
        }

        bool waitForLastTurnToFinish = false;
        while (matchContainer.lastTurnToPlay < previousTurns.Count)
        {
            Turn lastPrevTurn = previousTurns[matchContainer.lastTurnToPlay];

            if ((matchContainer.lastTurnToPlay == previousTurns.Count - 1) && !matchContainer.gameOver)
            {
                // l'ultimo turno ricevuto  ancora da giocare quindi esco dall'instant ed eseguo l'ultima mossa precedente come replay
                break;
            }

            int myTeamNum = teamManager.Team == TeamEnum.Team1 ? 1 : 2;
            int remoteTeamNum = RemoteArmyToMove(lastPrevTurn);

            int numUsefulTurns = matchContainer.gameOver ? previousTurns.Count : (previousTurns.Count - 1);

            if ((numUsefulTurns > 2)
                &&
                (
                (matchContainer.lastTurnToPlay == (numUsefulTurns - 1)) && (myTeamNum != remoteTeamNum)
                || (matchContainer.history && matchContainer.lastTurnToPlay > 1)
                ))
            {
                waitForLastTurnToFinish = true;
                ApplyRemoteTurn(lastPrevTurn);
                break;
            }
            else
            {
                InstantApplyRemoteTurn(lastPrevTurn);
                if ((numUsefulTurns > 2) && (matchContainer.lastTurnToPlay == (numUsefulTurns - 1)))
                {
                    TeamEnum winner = gameController.CheckGameOver();
                    bool gameOver = winner != TeamEnum.None;
                    if (gameOver)
                    {
                        pause = true;
                        remoteSimulation = true;
                    }
                }
            }

            if (matchContainer.lastTurnToPlay == 1) // turn zeroes ended
            {
                teamManager.AfterCharactersSpawn(); // init teams manager
            }

            matchContainer.lastTurnToPlay++;
        }

        if (!waitForLastTurnToFinish)
        {
            EndTurn(CurrentArmy() != RemoteArmyToMove(matchContainer.match.Turns[matchContainer.lastTurnToPlay]));
        }
    }

    public void HideEndTurnImage()
    {
        endTurnButton.enabled = false;
    }

    public void UpdateEndTurnImage()
    {
        endTurnButton.enabled = team == teamManager.Team;
    }

    public void EndTurnAndNext()
    {
        EndLocalTurn(matchContainer.GetCurrentTurn());
    }

    private void EndLocalTurn(Turn turn)
    {
        if (!remoteSimulation && !matchContainer.match.Ended)
        {
            SendTurn(turn);
        }
        matchContainer.lastTurnToPlay++;
        EndTurn(true);
    }

    private void EndRemoteTurnAndNext()
    {
        matchContainer.lastTurnToPlay++;
        EndTurn(true);
    }

    private void SendTurn(Turn turn)
    {
        inGameClient.SendTurn(turn);
    }

    private void NewTurnsCallback(int fromTurn)
    {
        AddRemoteTurnsAndApply();
    }

    private void SelectTeam(TeamEnum _team, bool triggerNext)
    {
        team = _team;

        if (teamManager.ControlBothTeams)
        {
            teamManager.Team = team;
        }
        else if (!matchContainer.history)
        {
            if (teamManager.Team == team)
            {
                inGamePoller.StopPollingOthersTurn();
            }
            else
            {
                inGamePoller.StartPollingOthersTurn(NewTurnsCallback);
            }
        }
        brush.ClearMarkers();
        teamManager.UpdateTurnDescriptions(team);
        //ResetTimer();
        pause = true;
        inputClickListen.DisableListen();
        // next function called after show team screen (trigger in animation)
        if (triggerNext)
        {
            teamScreen.ShowTeam(team);
        }
    }

    private void EndTurn(bool changeTeam)
    {
        teamManager.ResetTeamMoves(team);
        TeamEnum newTeam = team;
        if (changeTeam)
        {
            if (team == TeamEnum.Team1)
            {
                newTeam = TeamEnum.Team2;
            }
            else if (team == TeamEnum.Team2)
            {
                newTeam = TeamEnum.Team1;
            }
        }
        SelectTeam(newTeam, true);
    }

    public void NextRemote()
    {
        if (movesToApply.Count > 0)
        {
            Move move = movesToApply[0];
            movesToApply.Remove(move);
            teamManager.UpdateMoves();
            MakeMove(move);
        }
    }

    public void Next()
    {
        if (movesToApply.Count > 0)
        {
            inputClickListen.DisableListen();
            NextRemote();
            return;
        }
        else if (matchContainer.lastTurnToPlay < matchContainer.LastPlayableTurn())
        {
            if (matchContainer.history || matchContainer.lastTurnToPlay < matchContainer.LastPlayableTurn() - 1)
            {
                EndRemoteTurnAndNext();
                Turn lastPrevTurn = matchContainer.match.Turns[matchContainer.lastTurnToPlay];
                ApplyRemoteTurn(lastPrevTurn);
                return;
            }
        }

        bool localMove = !remoteSimulation;
        remoteSimulation = false;
        bool turnEnded = false;
        TeamEnum winner = gameController.CheckGameOver();
        bool gameOver = winner != TeamEnum.None;
        if (gameOver)
        {
            pause = true;
            if (localMove)
            {
                SendTurn(matchContainer.GetCurrentTurn());
            }
            return;
        }
        teamManager.UpdateMoves();
        if (localMove && !gameOver)
        {
            if (!teamManager.CanTeamMove(team))
            {
                EndTurnAndNext();
                turnEnded = true;
            }
            if (!matchContainer.history)
            {
                inputClickListen.EnableListen();
            }
        }
        else
        {
            EndRemoteTurnAndNext();
            turnEnded = true;
        }
        if (teamManager.Team == team && !matchContainer.history && !gameOver)
        {
            brush.MarkUsableCharacters(team, !turnEnded);
        }
    }

    //private void ResetTimer()
    //{
    //    if (teamManager.Team == team)
    //    {
    //        timer = timerSeconds;
    //        Color newColor = new Color(timerText.color.r, timerText.color.g, timerText.color.b, 1.0f);
    //        timerText.color = newColor;
    //    }
    //    else
    //    {
    //        timer = -1.0f;
    //        Color newColor = new Color(timerText.color.r, timerText.color.g, timerText.color.b, 0.0f);
    //        timerText.color = newColor;
    //    }
    //    UpdateTimerText();
    //}

}
