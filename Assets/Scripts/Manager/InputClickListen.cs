using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputClickListen : MonoBehaviour
{
    private GridController grid;
    private TeamsManager teamManager;
    private TurnManager turnManager;
    private InputClick inputClick;

    private void Awake()
    {
        grid = FindObjectOfType<GridController>();
        teamManager = GetComponent<TeamsManager>();
        turnManager = GetComponent<TurnManager>();
        inputClick = FindObjectOfType<InputClick>();
    }

    public void EnableListen()
    {
        turnManager.UpdateEndTurnImage();
        //inputClick.ClearOnTileClick();
        inputClick.OnTileClick += ListenToClick;
    }

    public void DisableListen()
    {
        turnManager.HideEndTurnImage();
        //inputClick.ClearOnTileClick();
        inputClick.OnTileClick -= ListenToClick;
    }

    public void ListenToClick(Vector3Int clickPosition)
    {
        List<Character> characters = teamManager.FindLivingCharacters();
        foreach (Character character in characters)
        {
            if (grid.GetCellPosition(character.transform.position).Equals(clickPosition))
            {
                if (character.Team == turnManager.Team && character.Team == teamManager.Team && character.CanMoveOrAttack())
                {
                    character.ControlMe();
                    character.ShowMoves();
                    DisableListen();
                }
                break;
            }
        }
    }
}
