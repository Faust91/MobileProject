using Assets.Scripts.Network.NetworkModel;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchContainer : MonoBehaviour
{
    public Match match;
    public Player player;
    public int lastTurnToPlay = 0;
    public List<CharacterStats> characterStats;
    static private DateTime TIME_ZERO = new DateTime();
    public TeamEnum elapsedTimeForTeam = TeamEnum.None;
    public bool history = false;
    public bool gameOver = false;

    // SINGLETON STUFF STARTS HERE 
    private static MatchContainer _instance;
    public static MatchContainer Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            _instance.InitSingleton();
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void InitSingleton()
    {
        // PUT OTHER STUFF HERE...
    }
    // SINGLETON STUFF ENDS HERE 

    public int LastPlayableTurn()
    {
        int last = match.Turns.Count - 1;
        if (match.Turns[last].GetEnd().Equals(TIME_ZERO))
        {
            return last - 1;
        }
        else
        {
            return last;
        }
    }

    public Turn GetCurrentTurn()
    {
        return match.Turns[lastTurnToPlay];
    }
    public void ResetContainer()
    {
        match = null;
        player = null;
        lastTurnToPlay = 0;
        elapsedTimeForTeam = TeamEnum.None;
        history = false;
        gameOver = false;
    }

    private bool imHost()
    {
        return PrefsManager.Instance.GetUsername().Equals(match.PlayerHost.Nickname);
    }

    private bool isPlayerGuest(bool itsMe)
    {
        return (itsMe ^ imHost());
    }

    public double GetRemainingMinutes(bool itsMe)
    {
        double remainingMinutes = (GetRemainingSeconds(itsMe) / 60.0d);
        if (remainingMinutes <= 0.0d)
        {
            remainingMinutes = -1.0d;
        }
        return remainingMinutes;
    }

    public double GetRemainingSeconds(bool itsMe)
    {
        TeamsManager teamsManager = FindObjectOfType<TeamsManager>();
        TurnManager turnManager = FindObjectOfType<TurnManager>();

        double remainingTime = match.MaxTimePlayers;
        if (itsMe)
        {
            for (int i = 2; i < match.Turns.Count; i++) //il primo turno di setup viene saltato
            {
                Turn turn = match.Turns[i];

                if (PrefsManager.Instance.GetUsername().Equals(turn.ownerId)) //Se quel turno � mio sottraggo il tempo
                {
                    if (!turn.GetEnd().Equals(TIME_ZERO))
                    {
                        remainingTime -= (turn.GetEnd() - turn.GetBegin()).TotalSeconds;
                    }
                    else if (teamsManager.Team.Equals(turnManager.Team))
                    {
                        remainingTime -= (DateTime.UtcNow.ToUniversalTime() - turn.GetBegin()).TotalSeconds;
                    }
                }
            }
        }
        else
        {
            for (int i = 2; i < match.Turns.Count; i++)
            {
                Turn turn = match.Turns[i];

                if (!PrefsManager.Instance.GetUsername().Equals(turn.ownerId))
                {
                    if (!turn.GetEnd().Equals(TIME_ZERO))
                    {
                        remainingTime -= (turn.GetEnd() - turn.GetBegin()).TotalSeconds;
                    }
                }
            }
        }
        return remainingTime;
    }

    public string GetPlayerName(bool itsMe)
    {
        if (isPlayerGuest(itsMe))
        {
            return match.PlayerGuest.Nickname;
        }
        else
        {
            return match.PlayerHost.Nickname;
        }
    }

}
