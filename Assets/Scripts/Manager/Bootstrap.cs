using Assets.Scripts.Network.NetworkModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bootstrap : MonoBehaviour
{
    [SerializeField] private string loggingMenuSceneName = "LoggingMenuScene";
    [SerializeField] private string mainMenuSceneName = "MainMenuScene";
    private UnitClient unitClient;

    private void Awake()
    {
        unitClient = FindObjectOfType<UnitClient>();
    }

    private void Start()
    {
        DownloadUnitsAndProceed();
    }

    private void DownloadUnitsAndProceed()
    {
        unitClient.GetUnits(Proceed);
    }

    private void Proceed(bool ok)
    {
        if (ok)
        {
            // procedo con le scene successive
            if (IsUsernamePresent() && IsTokenPresent())
            {
                NetworkGateway.ForceToken(PrefsManager.Instance.GetToken());
                ChangeScene.Instance.GoToScene(mainMenuSceneName);
            }
            else
            {
                ChangeScene.Instance.GoToScene(loggingMenuSceneName);
            }
        }
    }

    private bool IsUsernamePresent()
    {
        return (PrefsManager.Instance.GetUsername() != null && PrefsManager.Instance.GetUsername().Length > 0);
    }

    private bool IsTokenPresent()
    {
        return (PrefsManager.Instance.GetToken() != null && PrefsManager.Instance.GetToken().Length > 0);
    }

}
