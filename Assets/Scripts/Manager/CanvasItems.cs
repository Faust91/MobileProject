using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasItems : MonoBehaviour
{

    // SINGLETON STUFF STARTS HERE 
    private static CanvasItems _instance;
    public static CanvasItems Instance { get { return _instance; } }

    private AudioSource backgroundMusic;

    public AudioClip introMainMenuMusic;
    public AudioClip loopMainMenuMusic;
    public AudioClip InBattleMusic;
    public AudioClip Victory;
    public AudioClip Defeat;

    private Coroutine MainMenuMusicCoroutine;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            _instance.InitSingleton();
        }
        backgroundMusic = GetComponent<AudioSource>();
        DontDestroyOnLoad(this.gameObject);
    }

    private void InitSingleton()
    {
        // PUT OTHER STUFF HERE...
        RefreshReferences();
    }
    // SINGLETON STUFF ENDS HERE 

    private Text infoText = null;
    private Text errorText = null;
    private List<Button> buttons = new List<Button>();

    private void Start()
    {
        PlayMainMenuMusic();
    }

    public void RefreshReferences()
    {
        infoText = null;
        errorText = null;
        buttons.Clear();
        infoText = FindObjectOfType<InfoMessage>()?.GetText();
        errorText = FindObjectOfType<ErrorMessage>()?.GetText();
        DeactivatableButton[] dynamicButtons = FindObjectsOfType<DeactivatableButton>();
        if (dynamicButtons != null && dynamicButtons.Length > 0)
        {
            for (int i = 0; i < dynamicButtons.Length; i++)
            {
                buttons.Add(dynamicButtons[i].GetButton());
            }
        }
    }

    public void SetInfoText(string text)
    {
        if (infoText != null)
        {
            infoText.text = text;
        }
    }

    public void SetErrorText(string text)
    {
        if (errorText != null)
        {
            errorText.text = text; ;
        }
    }

    public void SetButtonsEnabled(bool enabled)
    {
        foreach (Button button in buttons)
        {
            if (button != null)
            {
                button.interactable = enabled;
            }
        }
    }

    public void AddButton(Button button)
    {
        buttons.Add(button);
    }

    private void OnLevelWasLoaded(int level)
    {
        RefreshReferences();
    }

    public void PlayDefeat()
    {
        if (MainMenuMusicCoroutine != null)
        {
            StopCoroutine(MainMenuMusicCoroutine);
        }
        backgroundMusic.Stop();
        backgroundMusic.clip = Defeat;
        backgroundMusic.loop = false;
        backgroundMusic.Play();

    }

    public void PlayVictory()
    {
        if (MainMenuMusicCoroutine != null)
        {
            StopCoroutine(MainMenuMusicCoroutine);
        }
        backgroundMusic.Stop();
        backgroundMusic.clip = Victory;
        backgroundMusic.loop = false;
        backgroundMusic.Play();

    }

    public void PlayBattleMusic()
    {
        if (MainMenuMusicCoroutine != null)
        {
            StopCoroutine(MainMenuMusicCoroutine);
        }
        StopCoroutine(MainMenuCoroutine());
        backgroundMusic.Stop();
        backgroundMusic.clip = InBattleMusic;
        backgroundMusic.loop = true;
        backgroundMusic.Play();
    }

    public void PlayMainMenuMusic()
    {
        if (MainMenuMusicCoroutine != null)
        {
            StopCoroutine(MainMenuMusicCoroutine);
        }
        MainMenuMusicCoroutine = StartCoroutine(MainMenuCoroutine());
    }

    public void StopMusic()
    {
        if (MainMenuMusicCoroutine != null)
        {
            StopCoroutine(MainMenuMusicCoroutine);
        }
        backgroundMusic.Stop();
    }

    private IEnumerator MainMenuCoroutine()
    {
        backgroundMusic.Stop();
        backgroundMusic.clip = introMainMenuMusic;
        backgroundMusic.loop = false;
        backgroundMusic.Play();
        yield return new WaitForSeconds(backgroundMusic.clip.length);
        backgroundMusic.Stop();
        backgroundMusic.clip = loopMainMenuMusic;
        backgroundMusic.loop = true;
        backgroundMusic.Play();
    }
}
