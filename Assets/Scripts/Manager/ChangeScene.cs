using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    // SINGLETON STUFF STARTS HERE 
    private static ChangeScene _instance;
    public static ChangeScene Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            _instance.InitSingleton();
        }
        DontDestroyOnLoad(this.gameObject);
    }
    private void InitSingleton()
    {
        // PUT OTHER STUFF HERE...
    }
    // SINGLETON STUFF ENDS HERE 

    public void GoToScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

}
