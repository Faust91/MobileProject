using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomToGrid : MonoBehaviour
{
    private GridInfo gridInfo;

    private void Awake()
    {
        gridInfo = FindObjectOfType<GridInfo>();
    }

    private void Start()
    {
        float gridW = gridInfo.HalfWidth;
        float gridH = gridInfo.HalfHeight;
        Camera.main.orthographicSize = gridH;
        if (gridH * Camera.main.aspect < gridW)
        {
            Camera.main.orthographicSize = gridW / Camera.main.aspect;
        }
    }
}
