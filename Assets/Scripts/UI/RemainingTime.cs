using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemainingTime : MonoBehaviour
{
    private MatchContainer matchContainer;
    [SerializeField] private bool itsMe;
    private Text text;
    static private float SECONDS_TO_WAIT = 10.0f;
    private float passedSeconds = 0.0f;

    private void Awake()
    {
        matchContainer = MatchContainer.Instance;
        text = GetComponent<Text>();
    }

    private void Start()
    {
        UpdateTimeText(itsMe);
    }

    private void Update()
    {
        passedSeconds += Time.deltaTime;
        if (passedSeconds >= SECONDS_TO_WAIT)
        {
            passedSeconds = 0.0f;
            UpdateTimeText(itsMe);
        }
    }

    private void UpdateTimeText(bool itsMe)
    {
        string nameToShow = matchContainer.GetPlayerName(itsMe);
        nameToShow = nameToShow.Substring(0, Mathf.Min(nameToShow.Length, 10));
        long remainingTime = (long)matchContainer.GetRemainingMinutes(itsMe);
        text.text = "" + nameToShow + ": " + (remainingTime >= 0L ? remainingTime : 0L) + " min remaining";
    }

    public bool IsItMe()
    {
        return itsMe;
    }

}
