using Assets.Scripts.Network.NetworkModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomListElem : MonoBehaviour
{
    [Header("Context")]
    [SerializeField] private Image teamImage;
    [SerializeField] private Text teamText;
    [SerializeField] private Text matchText;
    [SerializeField] private Text opponentText;
    [SerializeField] private GameObject gameOverLabel;
    [SerializeField] private Button matchButton;
    [SerializeField] private GameObject deleteButtonGO;
    [SerializeField] private Button deleteButton;

    private MatchClient matchCreatorClient;
    private MatchResume match;
    private string playerName;
    private bool joined;
    private bool history;
    private float teamColorAlpha = 0.375f;
    private RoomList roomList;

    public string GetMatchId()
    {
        return match?.Id;
    }

    private void Awake()
    {
        matchCreatorClient = FindObjectOfType<MatchClient>();
        deleteButton = deleteButtonGO?.GetComponent<Button>();
    }

    private void Start()
    {
        roomList = GetComponentInParent<RoomList>();
    }

    public void Init(MatchResume _match, string _playerName, bool _joined, bool _history)
    {
        match = _match;
        playerName = _playerName;
        joined = _joined;
        history = _history;
    }

    public void UpdateView()
    {
        matchText.text = match.MapName + ": " + match.MatchName;

        bool isItMine = match.PlayerHost == playerName;

        Army myArmy;

        if (isItMine)
        {
            myArmy = match.PlayerHostArmy;
            opponentText.text = match.PlayerGuest;
        }
        else
        {
            myArmy = match.PlayerGuestArmy;
            opponentText.text = match.PlayerHost;
        }

        switch (myArmy)
        {
            case Army.Humans:
                teamImage.color = new Color(0, 0, 1, teamColorAlpha);
                teamText.text = "Team1";
                break;
            case Army.Monsters:
                teamImage.color = new Color(1, 0, 0, teamColorAlpha);
                teamText.text = "Team2";
                break;
            default:
                Debug.LogError("Unknown kind of army!");
                teamImage.color = new Color(1, 1, 1, teamColorAlpha);
                teamText.text = "";
                break;
        }

        gameOverLabel.SetActive(match.Ended);
        deleteButtonGO.SetActive(joined);
    }

    public void JoinMatch()
    {
        if (joined)
        {
            matchCreatorClient.EnterMatch(match.Id, history);
        }
        else
        {
            roomList.JoinMatch(match, matchButton);
        }
    }

    public void DeleteMatch()
    {
        if (joined)
        {
            roomList.DeleteMatch(match, matchButton, deleteButton);
        }
    }

}
