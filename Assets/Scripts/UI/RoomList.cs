using Assets.Scripts.Network.NetworkModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomList : MonoBehaviour
{
    [SerializeField] private GameObject ListElemPrefab;
    private MatchClient matchClient;
    [SerializeField] private bool myMatches;
    [SerializeField] private bool history;
    private MatchPoller matchPoller;

    private void Awake()
    {
        matchClient = FindObjectOfType<MatchClient>();
        matchPoller = FindObjectOfType<MatchPoller>();
    }

    private void Start()
    {
        if (history)
        {
            matchClient.ShowHistory();
            matchPoller.StartPollingMyMatchesHistory();
        }
        else if (myMatches)
        {
            matchClient.ShowMyMatches();
            matchPoller.StartPollingMyMatches();
        }
        else
        {
            matchClient.ShowAvailableMatches();
            matchPoller.StartPollingOthersMatches();
        }
    }

    public void ClearList()
    {
        RoomListElem[] elems = gameObject.GetComponentsInChildren<RoomListElem>();
        if (elems != null && elems.Length > 0)
        {
            foreach (RoomListElem elem in elems)
            {
                Destroy(elem.gameObject);
            }
        }
    }

    public void RefreshList(List<MatchResume> matches, bool joined)
    {
        string username = PrefsManager.Instance.GetUsername();
        foreach (MatchResume match in matches)
        {
            GameObject obj = Instantiate(ListElemPrefab, Vector3.zero, Quaternion.identity, transform);
            RoomListElem elem = obj.GetComponent<RoomListElem>();
            elem.Init(match, username, joined, history);
            elem.UpdateView();
        }
    }

    public void DestroyElem(string matchId)
    {
        RoomListElem[] elems = gameObject.GetComponentsInChildren<RoomListElem>();
        if (elems != null && elems.Length > 0)
        {
            foreach (RoomListElem elem in elems)
            {
                if (matchId == elem.GetMatchId())
                {
                    Destroy(elem.gameObject);
                    return;
                }
            }
        }
    }

    public void JoinMatch(MatchResume match, Button joinButton)
    {
        matchClient.JoinMatch(match.Id, joinButton);
    }

    public void DeleteMatch(MatchResume match, Button enterButton, Button deleteButton)
    {
        matchClient.DeleteMatch(match.Id, enterButton, deleteButton);
    }

}
